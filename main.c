/**
 * @file main.c
 * @brief Lab 8.2 Sound In Sound Out using Ping Pong Buffer
 * @details
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "common_header.h"
#include "ADC_BF.h"
#include "InternalADC_BF.h"
#include "DAC_BF.h"
#include "SwitchesLEDs_BF.h"
#include "Timer_BF.h"
#include "SRAM_BF.h"
#include "LCD_Helper_Functions_BF.h"
#include "Signal_Processing_BF.h"
//#include "Delays_BF.h"
#include "DMA_BF.h"
#include "PingPongBuffer.h"
#include "EffectsBox.h"
#include "RotarySwitch.h"
#include "Encoder.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

static void Init_Devices();
static void updateLCD();
static void updateTopLineLCD(void);
static void updateBottomLineLCD(void);

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief Main Function
 */
int main(void)
{
    InitSysCtrl(); //Disable Watchdog, initialize SysCLK and Peripheral Clock
    Init_Devices();

    DFT_STRUCT dft_bf;
    SignalProcess_DFTInit(&dft_bf);

    StartCpuTimer1();
    StartCpuTimer0();
    //StartCpuTimer2();

    EnableInterrupts();

    while(1)
    {
        updateEffectVariable();
        if (updateToLCD)
        {
            updateTopLineLCD();
            updateBottomLineLCD();
            updateToLCD = false;
        }
        if(Check_Condition)
        {
            GPIO_Toggle(14);


            Check_Condition = 0;
            GPIO_Toggle(14);
        }

            RotarySwitch_DecodeInput();
            checkRotarySwitch = false;

    }
}


/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief Initialize Devices Function
 */
static void Init_Devices()
{
    SwitchesLEDsInit();
    ADC_Init();
    DAC_Init();
    InitPll(10, 3);
    DMA_Init();
    GPIO_InitDiagPort(14);
    GPIO_InitDiagPort(13);

    SRAM_Init();
    SignalProcess_Init();
    LCD_Init();
    InternalADC_Init();
    Buttons_Init();
    EffectsBox_Init();
    RotarySwitch_Init();
    Encoder_Init();

    ConfigCpuTimer(&CpuTimer0, 149.92, (effectTime*1000));
    Timer0_Interrupt_Init();
    TimerInUse.timerState = Timer_44100Hz;
}


static void updateLCD()
{
    // Display First Line
    LCD_Init();
    LCD_Clear_Screen();
    if (effectState == Effect_Delay)
    {
        LCD_Write_String("Delay  ");
    }
    else if (effectState == Effect_Flanger)
    {
        LCD_Write_String("Flanger  ");
    }
    else if (effectState == Effect_Phasor)
    {
        LCD_Write_String("Phasor  ");
    }
    else if (effectState == Effect_Transform)
    {
        LCD_Write_String("Transform  ");
    }
    else if (effectState == Effect_Reverb)
    {
        LCD_Write_String("Reverb  ");
    }
    else if (effectState == Effect_Roll)
    {
        LCD_Write_String("Roll  ");
    }
    else if (effectState == Effect_PitchShifter)
    {
        LCD_Write_String("Pitch Shifter  ");
    }
    // End of first line

    LCD_2nd_Line();


    if (showBars)
    {
        float scratch = 0;

        if (effectState == Effect_Delay)
        {
            scratch = effectTime *1000;      //convert to milliseconds
            if (375 <= scratch && scratch < 750)
            {
                LCD_Write_String(" ");
            }
            else if (187 <= scratch && scratch < 375)
            {
                LCD_Write_String("   ");
            }
            else if (187 <= scratch && scratch < 187)
            {
                LCD_Write_String("   ");
            }
            else if (93 <= scratch && scratch < 187)
            {
                LCD_Write_String("   ");
            }
        }
        else if (effectState == Effect_Flanger)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Phasor)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Transform)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Reverb)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Roll)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_PitchShifter)
        {
            LCD_Write_String("    ");
            LCD_Write_SingleBlock();
        }
    }
    else    // !showBars
    {
        float time = effectTime*1000;
        // Time should take first 8 spaces on 2nd row
        if (time < 1)
        {
            LCD_Write_Number((Uint16)(time*1000));
            LCD_Write_String("ns");
            // fill in blank space
            for (LCD_Pos; LCD_Pos < 8; LCD_Pos)
            {
                LCD_Write_String(" ");
            }
        }
        else
        {
            LCD_Write_Number((Uint16)(time));
            LCD_Write_String("ms");
            // fill in blank space
            for (LCD_Pos; LCD_Pos < 8; LCD_Pos)
            {
                LCD_Write_String(" ");
            }
        }
    }
    // Now draw  2nd part of 2nd line -  the block diagram using spaces 9-15
    switch (blockState)
    {
    case(Block_LLL):
        LCD_Write_SingleBlock();
        LCD_Write_String("__-___");
    break;
    case(Block_LLL_LL):
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("_-___");
    break;
    case(Block_LL):
        LCD_Write_String("_");
        LCD_Write_SingleBlock();
        LCD_Write_String("_-___");
    break;
    case(Block_LL_L):
        LCD_Write_String("_");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("-___");
    break;
    case(Block_L):
        LCD_Write_String("__");
        LCD_Write_SingleBlock();
        LCD_Write_String("-___");
    break;
    case(Block_L_C):
        LCD_Write_String("__");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("___");
    break;
    case(Block_C):
        LCD_Write_String("___");
        LCD_Write_SingleBlock();
        LCD_Write_String("___");
    break;
    case(Block_C_R):
        LCD_Write_String("___");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("__");
    break;
    case(Block_R):
        LCD_Write_String("___-");
        LCD_Write_SingleBlock();
        LCD_Write_String("__");
    break;
    case(Block_R_RR):
        LCD_Write_String("___-");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("_");
    break;
    case(Block_RR):
        LCD_Write_String("___-_");
        LCD_Write_SingleBlock();
        LCD_Write_String("_");
    break;
    case(Block_RR_RRR):
        LCD_Write_String("___-_");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
    break;
    case(Block_RRR):
        LCD_Write_String("___-__");
        LCD_Write_SingleBlock();
    break;
    case(Block_HighOutside):
    case(Block_LowOutside):
        LCD_Write_String("___-___");
    break;
    }
}

void updateTopLineLCD(void)
{
    // Display First Line
    LCD_Init();
    LCD_Clear_Screen();
    if (effectState == Effect_Delay)
    {
        LCD_Write_String("Delay  ");
    }
    else if (effectState == Effect_Flanger)
    {
        LCD_Write_String("Flanger  ");
    }
    else if (effectState == Effect_Phasor)
    {
        LCD_Write_String("Phasor  ");
    }
    else if (effectState == Effect_Transform)
    {
        LCD_Write_String("Transform  ");
    }
    else if (effectState == Effect_Reverb)
    {
        LCD_Write_String("Reverb  ");
    }
    else if (effectState == Effect_Roll)
    {
        LCD_Write_String("Roll  ");
    }
    else if (effectState == Effect_PitchShifter)
    {
        LCD_Write_String("Pitch Shifter  ");
    }
    // End of first line
}


void updateBottomLineLCD(void)
{
    LCD_2nd_Line();


    if (showBars)
    {
        float scratch = 0;

        if (effectState == Effect_Delay)
        {
            scratch = effectTime *1000;      //convert to milliseconds
            if (375 <= scratch && scratch < 750)
            {
                LCD_Write_String(" ");
            }
            else if (187 <= scratch && scratch < 375)
            {
                LCD_Write_String("   ");
            }
            else if (187 <= scratch && scratch < 187)
            {
                LCD_Write_String("   ");
            }
            else if (93 <= scratch && scratch < 187)
            {
                LCD_Write_String("   ");
            }
        }
        else if (effectState == Effect_Flanger)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Phasor)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Transform)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Reverb)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_Roll)
        {
            LCD_Write_String("   ");
            LCD_Write_SingleBlock();
        }
        else if (effectState == Effect_PitchShifter)
        {
            LCD_Write_String("    ");
            LCD_Write_SingleBlock();
        }
    }
    else    // !showBars
    {
        if (effectState == Effect_Reverb)
        {
            LCD_Write_Number(reverbPercent);
            LCD_Write_String("%");
            for (LCD_Pos; LCD_Pos < 8; LCD_Pos)
            {
                LCD_Write_String(" ");
            }
        }
        else
        {

            float time = effectTime*1000;
            // Time should take first 8 spaces on 2nd row
            if (time < 1)
            {
                LCD_Write_Number((Uint16)(time*1000));
                LCD_Write_String("ns");
                // fill in blank space
                for (LCD_Pos; LCD_Pos < 8; LCD_Pos)
                {
                    LCD_Write_String(" ");
                }
            }
            else
            {
                LCD_Write_Number((Uint16)(time));
                LCD_Write_String("ms");
                // fill in blank space
                for (LCD_Pos; LCD_Pos < 8; LCD_Pos)
                {
                    LCD_Write_String(" ");
                }
            }
        }
    }
    // Now draw  2nd part of 2nd line -  the block diagram using spaces 9-15
    switch (blockState)
    {
    case(Block_LLL):
        LCD_Write_SingleBlock();
        LCD_Write_String("__-___");
    break;
    case(Block_LLL_LL):
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("_-___");
    break;
    case(Block_LL):
        LCD_Write_String("_");
        LCD_Write_SingleBlock();
        LCD_Write_String("_-___");
    break;
    case(Block_LL_L):
        LCD_Write_String("_");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("-___");
    break;
    case(Block_L):
        LCD_Write_String("__");
        LCD_Write_SingleBlock();
        LCD_Write_String("-___");
    break;
    case(Block_L_C):
        LCD_Write_String("__");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("___");
    break;
    case(Block_C):
        LCD_Write_String("___");
        LCD_Write_SingleBlock();
        LCD_Write_String("___");
    break;
    case(Block_C_R):
        LCD_Write_String("___");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("__");
    break;
    case(Block_R):
        LCD_Write_String("___-");
        LCD_Write_SingleBlock();
        LCD_Write_String("__");
    break;
    case(Block_R_RR):
        LCD_Write_String("___-");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
        LCD_Write_String("_");
    break;
    case(Block_RR):
        LCD_Write_String("___-_");
        LCD_Write_SingleBlock();
        LCD_Write_String("_");
    break;
    case(Block_RR_RRR):
        LCD_Write_String("___-_");
        LCD_Write_SingleBlock();
        LCD_Write_SingleBlock();
    break;
    case(Block_RRR):
        LCD_Write_String("___-__");
        LCD_Write_SingleBlock();
    break;
    case(Block_HighOutside):
    case(Block_LowOutside):
        LCD_Write_String("___-___");
    break;
    }
}

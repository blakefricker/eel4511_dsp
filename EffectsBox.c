/**
 * @file EffectsBox.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "EffectsBox.h"
#include "Signal_Processing_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

Uint16 BPM;
bool reloadInputArray;
bool runDFT;
bool effectOn;
bool updateToLCD;
bool showBars;
Uint32 rollStartIndex;
Uint32 rollEndIndex;
Uint32 rollIndex;
Uint32 samplesPerChunk;
EFFECT_STATE effectState;
BLOCK_STATE blockState;
/* @brief EffectVolume - value between 0-1 */
float effectVolume;
/* @brief EffectTime - value in ms */
float effectTime;
float maxEffectTime;
float minEffectTime;
float defaultEffectTime;

#pragma DATA_SECTION(inputBuffer, ".SRAMData")
#pragma DATA_SECTION(outputBuffer, ".SRAMData")
int16 inputBuffer[BUFFER_ARRAY_LENGTH];
int16 outputBuffer[BUFFER_ARRAY_LENGTH];

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

void EffectsBox_Init(void)
{
    BPM = 120;
    reloadInputArray = false;
    runDFT = false;
    effectOn = false;
    effectVolume = 1;
    effectState = Effect_Flanger; //readRotarySwitchState();
    defaultEffectTime = EFFECT_FLANGER_DEFAULT_TIME;
    effectTime = defaultEffectTime;
    blockState = Block_C;
    showBars = false;
    rollStartIndex = 0;
    updateToLCD = true;
    bufferCounter = 0;


    for (Uint32 i = 0; i < BUFFER_ARRAY_LENGTH; i++)
    {
        inputBuffer[i] = 0;
        outputBuffer[i] = 0;
    }
}


/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @file ADC_BF.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "ADC_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

static void delay(long counter);

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief ADC Initialization Function
 */
void ADC_Init(void)
{
    InitMcbspb();

    EALLOW;

    //Configure GPIO Pins for McBSP-B using GPIO regs
    GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 3;    // GPIO25 is MDRB pin
    GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 3;    // GPIO26 is MCLKXB pin
    GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 3;    // GPIO27 is MFSXB pin

    GpioCtrlRegs.GPAPUD.bit.GPIO25 = 0;     // Enable pull-up on GPIO25 (MDRB)
    GpioCtrlRegs.GPAPUD.bit.GPIO26 = 0;     // Enable pull-up on GPIO26 (MCLKXB)
    GpioCtrlRegs.GPAPUD.bit.GPIO27 = 0;     // Enable pull-up on GPIO27 (MFSXB)

    GpioCtrlRegs.GPAQSEL2.bit.GPIO25 = 3;   // Asynch input GPIO25 (MDRB)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO26 = 3;   // Asynch input GPIO26(MCLKXB)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO27 = 3;   // Asynch input GPIO27 (MFSXB)

    //Configure McBSP registers
    McbspbRegs.SPCR2.all = 0x0000;      // Reset FS generator, sample rate generator & transmitter
    McbspbRegs.SPCR1.all = 0x0000;      // Reset Receiver, Right justify word
    McbspbRegs.SPCR1.bit.CLKSTP = 2;    // Clock stop mode, without clock delay

    McbspbRegs.PCR.bit.CLKRP = 1;       // Receive data is sampled on the rising edge
    McbspbRegs.PCR.bit.CLKXM = 1;       // The McBSP is a master in the SPI protocol.
    delay(500);                // Wait at least 2 SRG clock cycles
    McbspbRegs.PCR.bit.SCLKME = 0;      // SCLKME bit used in conjunction with
    McbspbRegs.SRGR2.bit.CLKSM = 1;     // CLKSM bit (0:1) selects LSPCLK as input clock.
    McbspbRegs.SRGR1.bit.CLKGDV = 10;

    McbspbRegs.PCR.bit.FSXM = 1;        // Transmit frame-synch mode (internally)

    McbspbRegs.SRGR2.bit.FSGM = 0;

    McbspbRegs.PCR.bit.FSXP = 1;        // Transmit frame-synch polarity (active-low)

    McbspbRegs.XCR2.bit.XDATDLY = 1;
    McbspbRegs.RCR2.bit.RDATDLY = 1;

    McbspbRegs.XCR1.bit.XFRLEN1 = 0;    // Transmit frame length = 1 words
    McbspbRegs.XCR1.bit.XWDLEN1 = 4;    // 24-bit word

    McbspbRegs.RCR1.bit.RFRLEN1 = 0;    // Receive frame length = 1 words
    McbspbRegs.RCR1.bit.RWDLEN1 = 4;    // 24-bit word

    McbspbRegs.SPCR2.bit.GRST = 1;      // Enable the sample rate generator
    delay(500);                           // Wait at least 2 CLKG cycles

    McbspbRegs.SPCR2.bit.XRST = 1;      // Release TX from reset
    McbspbRegs.SPCR1.bit.RRST = 1;      // Release RX from reset

    McbspbRegs.SPCR2.bit.FRST = 1;      // Enable framesync generator
}

///**
// * @brief Read ADC
// * @return float value between 0-3.3
// */
//float ADC_Read_float(void)
//{
//    McbspbRegs.DXR2.all = 1;
//    McbspbRegs.DXR1.all = 1;
//
//    while(McbspbRegs.SPCR1.bit.RRDY != 1){}
//
//    Uint16 retVal2 = McbspbRegs.DRR2.all;
//    Uint16 retVal1 = McbspbRegs.DRR1.all;
//
//    retVal2 <<= 14;
//    retVal1 >>= 2;
//
//    retVal1 |= retVal2;
//
//    //float retVal = retVal1 * 3.3;
//    //retVal /= 65535.0;
//
//    return (float)retVal1;
//}

/**
 * @brief Read ADC
 * @return float value between 0-3.3
 */
Uint16 ADC_Read_Uint16(void)
{
    McbspbRegs.DXR2.all = 1;
    McbspbRegs.DXR1.all = 1;

    while(McbspbRegs.SPCR1.bit.RRDY != 1){}

    Uint16 retVal2 = McbspbRegs.DRR2.all;
    Uint16 retVal1 = McbspbRegs.DRR1.all;

    retVal2 <<= 14;
    retVal1 >>= 2;

    retVal1 |= retVal2;

    return retVal1;
}


/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief Generic Delay Function
 * @param counter is the counter for the for loop.
 * @details This delay has not been calibrated for correct timing.
 */
static void delay(long counter)
{
    for (volatile long i = 0; i < counter; i++) {}
}

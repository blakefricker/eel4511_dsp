/**
 * @file DAC_BF.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "DAC_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

void DAC_Init(void)
{
    InitMcbspa();

    EALLOW;

    SysCtrlRegs.PCLKCR0.bit.MCBSPAENCLK = 1;

    //Configure GPIO Pins for McBSP-B using GPIO regs
    GpioCtrlRegs.GPAMUX2.bit.GPIO20 = 2;    // GPIO12 is MDXA pin
    GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 2;    // GPIO14 is MCLKXA pin
    GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 2;    // GPIO27 is MFSXA pin

    GpioCtrlRegs.GPAPUD.bit.GPIO20 = 0;     // Enable pull-up on GPIO20 (MDXA)
    GpioCtrlRegs.GPAPUD.bit.GPIO22 = 0;     // Enable pull-up on GPIO22 (MCLKXA)
    GpioCtrlRegs.GPAPUD.bit.GPIO23 = 0;     // Enable pull-up on GPIO23 (MFSXA)

    GpioCtrlRegs.GPAQSEL2.bit.GPIO20 = 3;   // Asynch input GPIO20 (MDXA)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO22 = 3;   // Asynch input GPIO22(MCLKXA)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO23 = 3;   // Asynch input GPIO23 (MFSXA)

    //Configure McBSP registers
    McbspaRegs.SPCR2.all = 0x0000;      // Reset FS generator, sample rate generator & transmitter
    McbspaRegs.SPCR1.all = 0x0000;      // Reset Receiver, Right justify word
    McbspaRegs.SPCR1.bit.CLKSTP = 2;    // Clock stop mode, without clock delay

    McbspaRegs.PCR.bit.CLKXP = 1;       // Transmit data is sampled on rising edge.
    McbspaRegs.PCR.bit.CLKRP = 0;       // Receive data is sampled on the rising edge
    McbspaRegs.PCR.bit.CLKXM = 1;       // The McBSP is a master in the SPI protocol.
    DELAY_US(500);                // Wait at least 2 SRG clock cycles
    McbspaRegs.PCR.bit.SCLKME = 0;      // SCLKME bit used in conjunction with
    McbspaRegs.SRGR2.bit.CLKSM = 1;     // CLKSM bit (0:1) selects LSPCLK as input clock.
    McbspaRegs.SRGR1.bit.CLKGDV = 10;

    McbspaRegs.PCR.bit.FSXM = 1;        // Transmit frame-synch mode (internally)

    McbspaRegs.SRGR2.bit.FSGM = 0;

    McbspaRegs.PCR.bit.FSXP = 1;        // Transmit frame-synch polarity (active-low)

    McbspaRegs.XCR2.bit.XPHASE = 0;
    McbspaRegs.RCR2.bit.RPHASE = 0;

    McbspaRegs.XCR1.bit.XFRLEN1 = 0;    // Transmit frame length = 1 words
    McbspaRegs.XCR1.bit.XWDLEN1 = 2;    // 16-bit word

    McbspaRegs.RCR1.bit.RFRLEN1 = 0;    // Receive frame length = 1 words
    McbspaRegs.RCR1.bit.RWDLEN1 = 2;    // 24-bit word

    McbspaRegs.SPCR2.bit.GRST = 1;      // Enable the sample rate generator
    DELAY_US(500);                           // Wait at least 2 CLKG cycles

    McbspaRegs.SPCR2.bit.XRST = 1;      // Release TX from reset
    McbspaRegs.SPCR1.bit.RRST = 1;      // Release RX from reset
    DELAY_US(200);

    McbspaRegs.SPCR2.bit.FRST = 1;      // Enable framesync generator

}


/**
 * @brief Send 16 bit value out to DAC
 * @param outputValue - 16 bit value to send out.
 */
void DAC_SendOut(Uint16 outputValue)
{
    while(McbspaRegs.SPCR2.bit.XRDY != 1){}

    McbspaRegs.DXR1.all = outputValue;
}


/**
 * @brief Converts a floating point Voltage value to a Digital Value
 * @param voltage - Voltage to be converted.
 * @return value of voltage mapped between 0-65535
 * @details This will map 0-3.3V to 0x0-0xFFFF
 */
Uint16 DAC_ConvertVoltagetoDigital(float voltage)
{
    voltage *= 65535;             //sample = Vin * Resolution / Vref
    voltage /= 3.3;
    return (Uint16)voltage;
}

/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

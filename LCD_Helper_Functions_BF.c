/**
 * @file LCD_Helper_Functions.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "LCD_Helper_Functions_BF.h"
#include "OneToOneI2CDriver.h"
#include "DSP2833x_Examples.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

#define LCD_I2C_Addr (0x3F)

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

Uint16 LCD_Pos;

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

static void write_command_bytes(Uint16 * const byte, Uint16 length);
static void write_data_bytes(Uint16 * const byte, Uint16 length);

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief LCD Initialization Function
 */
void LCD_Init(void)
{
    I2C_O2O_Master_Init(LCD_I2C_Addr, 150, 100);
    Uint16 InitValues[] = {0x33, 0x32, 0x28, 0x0C, 0x01};
    write_command_bytes(InitValues, 5);
    LCD_Pos = 0;
}

/**
 * @brief LCD Write String Function
 * @param values - Pointer to first character in string to send.
 */
void LCD_Write_String(char * values)
{
    Uint16 i = 0;
    while(values[i] != '\0')
    {
        Uint16 scratch = (Uint16)values[i];
        write_data_bytes(&scratch, 1);
        i++;
    }
}

void LCD_Write_SingleBlock(void)
{
    Uint16 scratch = 0xFF;
    write_data_bytes(&scratch, 1);
}


/**
 * @brief LCD Write Number Function
 * @param value - Interger value < 99999;
 */
void LCD_Write_Number(Uint32 value)
{
    Uint16 scratch = value/10000;
    if (scratch > 0)
    {
        scratch += 0x30;
        write_data_bytes(&scratch, 1);
        value %= 10000;
    }

    scratch = value/1000;

        scratch += 0x30;
        write_data_bytes(&scratch, 1);
        value %= 1000;


    scratch = value/100;

        scratch += 0x30;
        write_data_bytes(&scratch, 1);
        value %= 100;


    scratch = value/10;

        scratch += 0x30;
        write_data_bytes(&scratch, 1);
        value %= 10;

    scratch = value;
    scratch += 0x30;
    write_data_bytes(&scratch, 1);
}


/**
 * @brief LCD Clear Screen
 */
void LCD_Clear_Screen(void)
{
    Uint16 command = 0x01;
    write_command_bytes(&command, 1);
    delay_loop();
    LCD_Pos = 0;
}

/**
 * @brief LCD Move Cursor to 2nd Line
 */
void LCD_2nd_Line(void)
{
    Uint16 command = 0xC0;
    write_command_bytes(&command, 1);
    LCD_Pos = 0;
}


/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief Write Commands to LCd in chunks of bytes
 * @param byte - Pointer to the first data byte
 * @param length - How many bytes to send
 */
static void write_command_bytes(Uint16 * const byte, Uint16 length)
{
    for (Uint16 i = 0; i < length; i++)
    {
        Uint16 scratch = byte[i];
        scratch &= 0x00F0;
        scratch |= 0x000C;
        I2C_O2O_SendBytes(&scratch, 1);

        scratch &= 0x00F0;
        scratch |= 0x0008;
        I2C_O2O_SendBytes(&scratch, 1);

        scratch = byte[i];
        scratch &= 0x000F;
        scratch <<= 4;
        scratch |= 0x000C;
        I2C_O2O_SendBytes(&scratch, 1);

        scratch &= 0x00F0;
        scratch |= 0x0008;
        I2C_O2O_SendBytes(&scratch, 1);
        DELAY_US(500);
    }
}

/**
 * @brief Write Data to LCD
 * @param byte - Pointer to the first data byte
 * @param length - How many bytes to send
 */
static void write_data_bytes(Uint16 * const byte, Uint16 length)
{
    for (Uint16 i = 0; i < length; i++)
    {
        Uint16 scratch = byte[i];
        scratch &= 0x00F0;
        scratch |= 0x000D;
        I2C_O2O_SendBytes(&scratch, 1);

        scratch &= 0x00F0;
        scratch |= 0x0009;
        I2C_O2O_SendBytes(&scratch, 1);

        scratch = byte[i];
        scratch &= 0x000F;
        scratch <<= 4;
        scratch |= 0x000D;
        I2C_O2O_SendBytes(&scratch, 1);

        scratch &= 0x00F0;
        scratch |= 0x0009;
        I2C_O2O_SendBytes(&scratch, 1);
    }
    LCD_Pos++;
}




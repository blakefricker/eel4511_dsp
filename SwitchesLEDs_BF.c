/**
 * @file SwitchesLEDs_BF.c
 * @brief Source file for Switches and LEDs
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "SwitchesLEDs_BF.h"
#include "EffectsBox.h"
#include "Signal_Processing_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

#define downButton      (15)
#define upButton        (16)
#define onOffButton     (17)

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief Initialize Switch and LEDS
 * @details LEDS         GPIO 0 - 7
 *          Switches     GPIO 8 - 11
 */
void SwitchesLEDsInit(void)
{
    EALLOW;
    GpioCtrlRegs.GPAMUX1.all = 0x00000000;  //set GPIO31:0 pins to GPIO
    GpioCtrlRegs.GPADIR.all |= 0xFF;     //set GPIO7:0 as output
    GpioCtrlRegs.GPAPUD.all &= 0xFFFFF0FF;
    Uint32 scratch = GpioDataRegs.GPADAT.all;
    scratch |= 0x00FF;
    GpioDataRegs.GPADAT.all = scratch;
}

/**
 * @brief Initialize three buttons for effects machine
 * @details Down   Button   GPIO 15     XINT1
 *          Up     Button   GPIO 16     XINT2
 *          On/Off Button   GPIO 17     XNMI
 */
void Buttons_Init(void)
{
    EALLOW;
    GpioCtrlRegs.GPAMUX1.all &= 0x3FFFFFFF;  //set GPIO15 to zero sets pins to GPIO
    GpioCtrlRegs.GPAMUX2.all &= 0xFFFFFFF0;  //set GPIO16:17 to zero sets pins to GPIO

    GpioCtrlRegs.GPADIR.all &= 0xFFFC7FFF;     //set GPIO15:17 as input
    GpioCtrlRegs.GPAPUD.all &= 0xFFFC7FFF;   //Enable Pullups (set bit to zero)

    //setup high to low transition interrupts
    GpioIntRegs.GPIOXINT1SEL.bit.GPIOSEL = downButton;    // set GPIO15 as interrupt source for XINT1
    GpioIntRegs.GPIOXINT2SEL.bit.GPIOSEL = upButton;
    GpioIntRegs.GPIOXNMISEL.bit.GPIOSEL = onOffButton;     // on/off switch set as non-maskable interrupt

    PieVectTable.XINT1 = &upButtonPressed_isr;
    PieVectTable.XINT2 = &downButtonPressed_isr;
    PieVectTable.XNMI = &onOffButtonPressed_isr;
    EDIS;

    //Qualification Period to prevent bouncing
//    GpioCtrlRegs.GPACTRL.bit.QUALPRD1 = 64;
//    GpioCtrlRegs.GPACTRL.bit.QUALPRD2 = 64;
//    GpioCtrlRegs.GPAQSEL1.bit.GPIO15 = 2;
//    GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 2;
//    GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 2;


    // Configure falling edge
    XIntruptRegs.XINT1CR.bit.POLARITY = 0;      // Falling edge interrupt
    XIntruptRegs.XINT2CR.bit.POLARITY = 0;      // Falling edge interrupt
    XIntruptRegs.XNMICR.bit.POLARITY = 0;      // Falling edge interrupt

    // Enable XINT1 and XINT2
    XIntruptRegs.XINT1CR.bit.ENABLE = 1;        // Enable Xint1
    XIntruptRegs.XINT2CR.bit.ENABLE = 1;        // Enable XINT2
    XIntruptRegs.XNMICR.bit.ENABLE = 1;        // Enable XINT2

    // Enable Xint1 and XINT2 in the PIE: Group 1 interrupt 4 & 5
    // Enable int1 which is connected to WAKEINT:
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;          // Enable the PIE block
    PieCtrlRegs.PIEIER1.bit.INTx4 = 1;          // Enable PIE Group 1 INT4
    PieCtrlRegs.PIEIER1.bit.INTx5 = 1;          // Enable PIE Group 1 INT5
    IER |= M_INT1;                              // Enable CPU int1
}


__interrupt void upButtonPressed_isr(void)
{
    switch(blockState)
    {
        case(Block_LowOutside):
            effectTime = defaultEffectTime/8;
            blockState++;
        break;
        case(Block_LLL):
        case(Block_LLL_LL):
            effectTime = defaultEffectTime/4;
            blockState = Block_LL;
        break;
        case(Block_LL):
        case(Block_LL_L):
            effectTime = defaultEffectTime/2;
            blockState = Block_L;
        break;
        case(Block_L):
        case(Block_L_C):
            effectTime = defaultEffectTime;
            blockState = Block_C;
        break;
        case(Block_C):
        case(Block_C_R):
            effectTime = defaultEffectTime*2;
            blockState = Block_R;
        break;
        case(Block_R):
        case(Block_R_RR):
            effectTime = defaultEffectTime*4;
            blockState = Block_RR;
        break;
        case(Block_RR):
        case(Block_RR_RRR):
            effectTime = defaultEffectTime*8;
            blockState = Block_RRR;
        break;
        case(Block_RRR):
        case(Block_HighOutside):
            effectTime = defaultEffectTime*16;
            blockState = Block_HighOutside;
    }

    rollIndex = rollStartIndex;
    if (effectTime > EFFECT_MAX_TIME)
    {
        effectTime == EFFECT_MAX_TIME;
    }

    updateToLCD = true;
    //showBars = true;
    // Acknowledge this interrupt to get more from group 1

    if (effectState == Effect_Phasor)
    {
        //CpuTimer0Regs.PRD.all =  effectTime;
        freq_c = 100;
    }
    else if (effectState == Effect_Flanger)
    {
        freq_c = 100;
    }
    else if (effectState == Effect_Reverb)
    {
        if (effectTime == defaultEffectTime/16)
        {
            reverbPercent = 10;
        }
        else if (effectTime == defaultEffectTime/8)
        {
            reverbPercent = 20;
        }
        else if (effectTime == defaultEffectTime/4)
        {
            reverbPercent = 30;
        }
        else if (effectTime == defaultEffectTime/2)
        {
            reverbPercent = 40;
        }
        else if (effectTime == defaultEffectTime)
        {
            reverbPercent = 50;
        }
        else if (effectTime == defaultEffectTime*2)
        {
            reverbPercent = 60;
        }
        else if (effectTime == defaultEffectTime*4)
        {
            reverbPercent = 70;
        }
        else if (effectTime == defaultEffectTime*8)
        {
            reverbPercent = 80;
        }
        else if (effectTime == defaultEffectTime*16)
        {
            reverbPercent = 90;
        }
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}


__interrupt void downButtonPressed_isr(void)
{
    switch(blockState)
    {
        case(Block_LowOutside):
        case(Block_LLL):
            if (effectTime <= defaultEffectTime/16)
            {
                effectTime = effectTime/2;
            }
            else
            {
                effectTime = defaultEffectTime/16;
            }
        break;

        case(Block_LLL_LL):
        case(Block_LL):
            effectTime = defaultEffectTime/8;
            blockState = Block_LLL;
        break;

        case(Block_LL_L):
        case(Block_L):
            effectTime = defaultEffectTime/4;
            blockState = Block_LL;
        break;

        case(Block_L_C):
        case(Block_C):
            effectTime = defaultEffectTime/2;
            blockState = Block_L;
        break;


        case(Block_C_R):
        case(Block_R):
            effectTime = defaultEffectTime;
            blockState = Block_C;
        break;

        case(Block_R_RR):
        case(Block_RR):
            effectTime = defaultEffectTime*2;
            blockState = Block_R;
        break;


        case(Block_RR_RRR):
        case(Block_RRR):
            effectTime = defaultEffectTime*4;
            blockState = Block_RR;
        break;


        case(Block_HighOutside):
            effectTime = defaultEffectTime*8;
            blockState--;
    }

    rollIndex = rollStartIndex;
    updateToLCD = true;
    //showBars = true;

    if (effectState == Effect_Phasor)
    {
        //CpuTimer0Regs.PRD.all =  effectTime;
        freq_c = 100;
    }
    else if (effectState == Effect_Flanger)
    {
        freq_c = 100;
    }
    else if (effectState == Effect_Reverb)
    {
        if (effectTime == defaultEffectTime/16)
        {
            reverbPercent = 10;
        }
        else if (effectTime == defaultEffectTime/8)
        {
            reverbPercent = 20;
        }
        else if (effectTime == defaultEffectTime/4)
        {
            reverbPercent = 30;
        }
        else if (effectTime == defaultEffectTime/2)
        {
            reverbPercent = 40;
        }
        else if (effectTime == defaultEffectTime)
        {
            reverbPercent = 50;
        }
        else if (effectTime == defaultEffectTime*2)
        {
            reverbPercent = 60;
        }
        else if (effectTime == defaultEffectTime*4)
        {
            reverbPercent = 70;
        }
        else if (effectTime == defaultEffectTime*8)
        {
            reverbPercent = 80;
        }
        else if (effectTime == defaultEffectTime*16)
        {
            reverbPercent = 90;
        }
    }
    // Acknowledge this interrupt to get more from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

__interrupt void onOffButtonPressed_isr(void)
{
    if (effectOn)
    {
        //Need to turn off
        effectOn = false;
        GpioDataRegs.GPADAT.all |= 0xFF;    //turn off indicator light (active low)
    }
    else
    {
        //Need to turn on
        effectOn = true;
        GpioDataRegs.GPADAT.all &= 0xFFFFFF00;   //turn on indicator light (active low)
        if (effectState == Effect_Roll)
        {
            rollStartIndex = (Uint32)(((int32)((float)bufferIndex - samplesPerChunk)) % BUFFER_ARRAY_LENGTH);
            rollEndIndex = bufferIndex;
            rollIndex = rollStartIndex;

        }
        else if (effectState == Effect_Flanger)
        {
            freq_c = 100;
        }

    }

    // Acknowledge this interrupt to get more from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}



/**
 * @brief Send value to light up LEDs
 * @param value - value to be lit up in binary on LEDs
 */
void sendToLEDs(int value)
{
    int scratch = GpioDataRegs.GPADAT.all;

    scratch &= 0xFF00;

    value = (~value);
    value &= 0x00FF;

    scratch |= value;

    GpioDataRegs.GPADAT.all = scratch;
}


/**
 * @brief Return the value read by switches
 * @return Value read by the switches
 */
int returnSwitchValue(void)
{
    int scratch = GpioDataRegs.GPADAT.all;

    scratch >>= 8;

    scratch &= 0x00F;

    int scratch2 = scratch << 4;

    scratch |= scratch2;

    return scratch;
}


/**
 * @brief Function that will return the switchVal variable
 * @details Provides structure to use switch (case) statement for switches.
 */
Uint16 checkSwitches(void)
{
    Uint16 switchVal = 0, prevSwitchVal = 0, prevSwitch0 = 0, prevSwitch2 = 0, prevSwitch3 = 0;

    //Read Switches
    switchVal = returnSwitchValue();
    switchVal &= 0x000F;

    // Check if switch has changed to reset flags
    if (switchVal != prevSwitchVal)
    {

        //Check switch 0
        Uint16 scratch = switchVal;
        scratch &= 0x1;
        if (scratch != prevSwitch0)
        {
            prevSwitch0 = scratch;
        }

        //Check switch 2
        scratch = switchVal;
        scratch &= 0x4;
        if (scratch != prevSwitch2)
        {
            if (scratch == 0x4)
            {
                // insert something here for switch 2
            }
            prevSwitch2 = scratch;
        }

        //Check switch 3
        scratch = switchVal;
        scratch &= 0x8;
        if (scratch == prevSwitch3)
        {
            //SRAM_Index = 0;
            prevSwitch3 = scratch;
        }
    }
    prevSwitchVal = switchVal;

    return switchVal;
}

/**
 * @brief Initialize a GPIO port for diag
 * @param portNumber the GPIO port to be initialized
 */
void GPIO_InitDiagPort(Uint16 portNumber)
{
    EALLOW;
    Uint32 bitmask = (1 << portNumber);
    GpioCtrlRegs.GPADIR.all |= bitmask;   //set GPIO as output

}

/**
 * @brief Toggle a GPIO
 * @param bitNumber the GPIO port to be toggled
 */
void GPIO_Toggle(Uint16 bitNumber)
{
    EALLOW;
    Uint32 scratch = GpioDataRegs.GPADAT.all;
    Uint32 copy = scratch;
    Uint32 bitmask = (1 << bitNumber);
    scratch &= bitmask;         //zero everything but the bit
    if (scratch == bitmask)     //bit was set
    {
        copy &= (~bitmask);     //set the bit to zero
    }
    else
    {
        copy |= (bitmask);      // set the bit to one
    }
    GpioDataRegs.GPADAT.all = copy;
}

void GPIO13_SetHigh()
{
    EALLOW;
    GpioDataRegs.GPADAT.all |= 0x2000;
}

void GPIO13_SetLow()
{
    EALLOW;
    GpioDataRegs.GPADAT.all &= 0xFFFFDFFF;
}

/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

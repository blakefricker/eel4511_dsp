
/**
 * @file Timer_BF.h
 * @brief TODO:
 * Author: Blake Fricker
 */

#ifndef TIMER_BF_H_
#define TIMER_BF_H_

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

//#include "Delays_BF.h"
/**
 *------------------------------------------------------------------------------
 * Public Defines
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Data Types
 *------------------------------------------------------------------------------
 */

typedef enum
{
    Timer_2_5KHz    =   (0x00),
    Timer_5KHz,
    Timer_10KHz,
    Timer_15KHz,
    Timer_20KHz,
    Timer_25KHz,
    Timer_30KHz,
    Timer_35KHz,
    Timer_40KHz,
    Timer_44100Hz,
    Timer_NumerOfStates
}TIMER_STATE;

typedef struct
{
    TIMER_STATE         timerState;
    //TIMER_STATE         timerPlaybackState;
    Uint32              timerFreq;
    //REVERB_DELAY_STATE  reverbDelayState;
    //float               reverbDelay_sec;
    //ECHO_DELAY_STATE    echoDelayState;
    //float               echoDelay_sec;
}TIMER_STRUCT;

/**
 *------------------------------------------------------------------------------
 * Public Variables (extern)
 *------------------------------------------------------------------------------
 */

extern Uint16 delayIndex1;
extern Uint16 delayIndex2;
extern Uint16 Check_Condition;
extern TIMER_STRUCT TimerInUse;

/**
 *------------------------------------------------------------------------------
 * Public Constants (extern)
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Prototypes
 *------------------------------------------------------------------------------
 */

//void Timer1_Init_mSec(Uint32 mSec);
//void Timer1_Init_uSec(float uSec);
void Timer1_Init_Gen(TIMER_STATE timerState);
void Timer0_Interrupt_Init(void);
void Timer2_Interrupt_Init(void);
//void Timer1_Interrupt_Init(void);
__interrupt void cpu_timer0_isr(void);
__interrupt void cpu_timer1_isr(void);
__interrupt void cpu_timer2_isr(void);


#endif /* TIMER_BF_H_ */


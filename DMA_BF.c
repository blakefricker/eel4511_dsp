/**
 * @file DMA_BF.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "DMA_BF.h"
#include "Timer_BF.h"
#include "PingPongBuffer.h"
#include "EffectsBox.h"
#include "Signal_Processing_BF.h"
#include <math.h>

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

bool PingMode;
#pragma DATA_SECTION(pingOut, "PingPong")
Uint16 pingOut[PINGPONG_BUFFER_LENGTH];
#pragma DATA_SECTION(pongOut, "PingPong")
Uint16 pongOut[PINGPONG_BUFFER_LENGTH];
#pragma DATA_SECTION(pingIn, "PingPong")
Uint16 pingIn[PINGPONG_BUFFER_LENGTH*2];
#pragma DATA_SECTION(pongIn, "PingPong")
Uint16 pongIn[PINGPONG_BUFFER_LENGTH*2];

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

const Uint16 garbageData = 0x5555;

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief DMA Channel 1 Initialization
 */
void DMA_Init(void)
{

    for (Uint16 i = 0; i < PINGPONG_BUFFER_LENGTH; i++)
    {
        pingOut[i] = 0;
        pongOut[i] = 0;
    }

    for (Uint16 i = 0; i < PINGPONG_BUFFER_LENGTH*2; i++)
    {
        pingIn[i] = 0;
        pongIn[i] = 0;
    }

    DMAInitialize();
    DmaRegs.DEBUGCTRL.bit.FREE = 0;
    DMACH1AddrConfig((volatile Uint16 *)&McbspbRegs.DXR2.all, (volatile Uint16 *)&garbageData);
    DMACH1BurstConfig(1,1,1);
    DMACH1TransferConfig(0,-1,-1);
    DMACH1ModeConfig(DMA_TINT1, PERINT_ENABLE, ONESHOT_DISABLE, CONT_ENABLE, \
            SYNC_DISABLE, SYNC_SRC, OVRFLOW_DISABLE, SIXTEEN_BIT, CHINT_BEGIN, CHINT_DISABLE);

    DMACH2AddrConfig((volatile Uint16 *)&pingIn[1], (volatile Uint16 *)&McbspbRegs.DRR2.all);
    DMACH2BurstConfig(1,1,-1);
    DMACH2TransferConfig(PINGPONG_BUFFER_LENGTH-1,-1,3);
    DMACH2ModeConfig(DMA_MREVTB, PERINT_ENABLE, ONESHOT_DISABLE, CONT_ENABLE, \
            SYNC_DISABLE, SYNC_SRC, OVRFLOW_DISABLE, SIXTEEN_BIT, CHINT_END, CHINT_ENABLE);

    DMACH3AddrConfig((volatile Uint16 *)&McbspaRegs.DXR1.all, (volatile Uint16 *)&pingOut[0]);
    DMACH3BurstConfig(0,0,0);
    DMACH3TransferConfig(PINGPONG_BUFFER_LENGTH-1,1,0);
    DMACH3ModeConfig(DMA_TINT1, PERINT_ENABLE, ONESHOT_DISABLE, CONT_ENABLE, \
            SYNC_DISABLE, SYNC_SRC, OVRFLOW_DISABLE, SIXTEEN_BIT, CHINT_END, CHINT_DISABLE);

    InitPieVectTable();
    InitPieCtrl();          //Set PIE to default state
    IER = 0x0000;           //Disable CPU Interrupts
    IFR = 0x0000;

    EALLOW;
    PieVectTable.DINTCH2 = &dataBufferFull_ISR;
//    PieVectTable.DINTCH3 = &startTransformISR;
//    PieVectTable.XINT13 = startTransformISR;

    PieCtrlRegs.PIEIER7.bit.INTx2 = 1;          // Enable PIE Interrupt 7.2
//    PieCtrlRegs.PIEIER7.bit.INTx3 = 1;          // Enable PIE Interrupt 7.3

//    XIntruptRegs.XNMICR.bit.ENABLE = 0;         // Timer1 Interrupt selected on mux
//    XIntruptRegs.XNMICR.bit.SELECT = 0;         // Timer1 Interrupt selected on mux

    IER |= M_INT7;                              // Enable Interrupt Group 7
//    IER |= M_INT13;                             // Enable Interrupt Group 13
    // Init Timer0
    //InitCpuTimers();                            // Initialize all 3 timers
    TimerInUse.timerState = Timer_44100Hz;      // Setup Timer1 for 44100Hz
    Timer1_Init_Gen(TimerInUse.timerState);

    // Start up Process
    StartDMACH1();
    StartDMACH2();
    StartDMACH3();

    // Start with Ping Buffers.
    PingMode = true;
}


__interrupt void dataBufferFull_ISR()
{
    EALLOW;

    // Swap the DMA Shadow Registers
    if (!PingMode)
    {
        DmaRegs.CH3.SRC_ADDR_SHADOW = (Uint32)&pingOut;
        DmaRegs.CH3.SRC_BEG_ADDR_SHADOW = (Uint32)&pingOut;

        DmaRegs.CH2.DST_ADDR_SHADOW = (Uint32)&pingIn[1];
        DmaRegs.CH2.DST_BEG_ADDR_SHADOW = (Uint32)&pingIn[1];

        PingMode = true;
    }
    else
    {
        DmaRegs.CH3.SRC_ADDR_SHADOW = (Uint32)&pongOut;
        DmaRegs.CH3.SRC_BEG_ADDR_SHADOW = (Uint32)&pongOut;

        DmaRegs.CH2.DST_ADDR_SHADOW = (Uint32)&pongIn[1];
        DmaRegs.CH2.DST_BEG_ADDR_SHADOW = (Uint32)&pongIn[1];

        PingMode = false;
    }

    // PIE ACK - allows this ISR to be called again.
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
    EDIS;


    // PingMode flag already flipped in ISR.
    Uint16 * input = !PingMode ? pingIn : pongIn;
    Uint16 * output = !PingMode ? pingOut : pongOut;

    if (effectState == Effect_Phasor)
    {
//        F_factor = (2*sinf(3.14 * (freq_c / TimerInUse.timerFreq)));
//        ConfigCpuTimer(&CpuTimer0, 149.92, (effectTime*1000));
        F_factor = sinf(2*3.14*freq_c);
        if (F_factor < 0)
        {
            F_factor *= (-1);
        }
        CpuTimer0.PeriodInUSec = (effectTime*1000);
        CpuTimer0Regs.PRD.all = (long) (149.92 * effectTime*1000);

    }
    else if(effectState == Effect_Roll)
    {
        samplesPerChunk = (TimerInUse.timerFreq * effectTime);
    }
    else if (effectState == Effect_Flanger)
    {
        F_factor = sinf(2*3.14*freq_c);
        if (F_factor < 0)
        {
            F_factor *= (-1);
        }
        CpuTimer0.PeriodInUSec = (effectTime*1000);
        CpuTimer0Regs.PRD.all = (long) (149.92 * effectTime*1000);
    }

    // Now copy the data from input to output
    for (Uint16 i = 0; i < PINGPONG_BUFFER_LENGTH; i++)
    {
        bufferIndex = (((float)bufferCounter*PINGPONG_BUFFER_LENGTH)+i);

        inputBuffer[bufferIndex] =  (input[i*2] >>2) | (input[i*2+1] << 14) - 0x7FFF;

        //Here perform the requested effect

        if (effectOn)
        {
            SignalProcessingIndex = i;
            // find and run correct effect
            if (effectState == Effect_Reverb)
            {
                SignalProcess_RealTimeReverb(inputBuffer, effectTime, effectVolume);
                output[i] = outputBuffer[bufferIndex] + 0x7FFF;
            }
            else if (effectState == Effect_Delay)
            {
                SignalProcess_RealTimeEcho(inputBuffer, outputBuffer, effectTime, effectVolume);
                output[i] = outputBuffer[bufferIndex] + 0x7FFF;
            }
            else if (effectState == Effect_Transform)
            {
                SignalProcess_RealTimeTransform(inputBuffer, effectTime, effectVolume);
                output[i] = outputBuffer[bufferIndex] + 0x7FFF;
            }
            else if (effectState == Effect_PitchShifter)
            {
                SignalProcess_RealTimeHamming(inputBuffer, effectTime, effectVolume);
                output[i] = outputBuffer[bufferIndex] + 0x7FFF;
            }
            else if (effectState == Effect_Roll)
            {
                SignalProcess_RealTimeRoll(outputBuffer, output, effectTime, effectVolume);
            }
            else if (effectState == Effect_Phasor)
            {
                SignalProcess_RealTimePhasor(inputBuffer, effectVolume);
                output[i] = outputBuffer[bufferIndex] + 0x7FFF;
            }
            else if (effectState == Effect_Flanger)
            {
                SignalProcess_RealTimeFlanger(inputBuffer, outputBuffer, effectTime, effectVolume);
                output[i] = outputBuffer[bufferIndex] + 0x7FFF;
            }
        }
        else // effect off - just save what is going out to the output buffer
        {
            outputBuffer[bufferIndex] = inputBuffer[bufferIndex];
            output[i] = outputBuffer[bufferIndex] + 0x7FFF;
        }
    }

    // End of For Loop Tasks
    bufferCounter++;
    if (bufferCounter >= (Uint16)BUFFER_MULTIPLIER)
    {
        bufferCounter = 0;
    }

    // Set Check Condition for main loop
    Check_Condition = true;
}

__interrupt void startTransformISR()
{
    // PIE ACK - allows this ISR to be called again.
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP7;
}
/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @file Signal_Processing_BF.h
 * @brief TODO:
 * Author: Blake Fricker
 */

#ifndef SIGNAL_PROCESSING_BF_H_
#define SIGNAL_PROCESSING_BF_H_

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include <DSP2833x_Device.h>
#include "SRAM_BF.h"
#include "PingPongBuffer.h"

/**
 *------------------------------------------------------------------------------
 * Public Defines
 *------------------------------------------------------------------------------
 */

#define SignalProcessingMaxIndex   (PINGPONG_BUFFER_LENGTH)    // 256K

///**@brief FIR Filter Structrue Defaults */
//#define DEFAULT_FIR     {   (const float*)0,        \
//                            (float*)0,              \
//                            0,                      \
//                            0,                      \
//                            SignalProcessingMaxIndex,          \
//                            SignalProcess_FIR       }
//
///**@brief IIR Filter Structrue Defaults */
//#define DEFAULT_IIR     {   (const float*)0,        \
//                            (const float*)0,        \
//                            (float*)0,              \
//                            0,                      \
//                            (float*)0,        \
//                            0,                      \
//                            SignalProcessingMaxIndex,          \
//                            0                      }
//
//
//#define LOWPASS_FIR_TAPS        (41)
//#define BANDPASS_FIR_TAPS       (160)
//#define HIGHPASS_FIR_TAPS       (5)
//
//#define LOWPASS_IIR_TAPS        (6)
//#define BANDPASS_IIR_TAPS       (7)
//
//
//
///** @brief IIR LowPass Filter Coefficients  */
//#define IIR16_LP_COEFF {\
//            -6356,14411,7,14,7,\
//            -7378,15424,10655,21311,10655}
//
//#define IIR16_LP_ISF   136
//#define IIR16_LP_NBIQ  2
//#define IIR16_LP_QFMAT 13
//
//
///**
// *------------------------------------------------------------------------------
// * Public Macros
// *------------------------------------------------------------------------------
// */
//
///**
// *------------------------------------------------------------------------------
// * Public Data Types
// *------------------------------------------------------------------------------
// */
//
///** @brief FIR Filter Structure */
//typedef struct fir_bf
//{
//    const float * coefficients_ptr;     //pointer to coeffiencts array
//    float * circularBuffer_ptr;         //point to circular buffer
//    Uint32 circularBuffer_Index;        //current circular buffer index
//    Uint32 taps;                        //Order of the filter
//    Uint32 circularBuffer_MaxIndex;      // Max Index value of circular buffer
//    float (*SignalProcess_FIR)(void *); //function pointer to asm function
//}FIR_STRUCT;
//
///** @brief IIR Filter Structure */
//typedef struct iir_bf
//{
//    const float * coefficientsA_ptr;     //pointer to coeffiencts array
//    const float * coefficientsB_ptr;     //pointer to coeffiencts array
//    float * circularBuffer_ptr;          //pointer to circular buffer (X)
//    Uint32 circularBuffer_Index;         //current circular buffer index
//    float * outputBuffer_ptr;            //pointer to output circular buffer (Y)
//    Uint32 taps;                         //Order of the filter
//    Uint32 circularBuffer_MaxIndex;      // Max Index value of circular buffer
//    float (*SignalProcess_IIR)(void *);  //function pointer to asm function
//}IIR_STRUCT;


/** @brief DFT Structure */
typedef struct
{
    float * mag;
    float * imag;
    float * real;
    int16 * input;
    float maxMag;
    Uint16 N;
    Uint16 maxIndex;
    Uint16 maxFreq;
}DFT_STRUCT;

/**
 *------------------------------------------------------------------------------
 * Public Variables (extern)
 *------------------------------------------------------------------------------
 */

extern float x_mag[SignalProcessingMaxIndex];
extern float x_imag[SignalProcessingMaxIndex];
extern float x_real[SignalProcessingMaxIndex];
extern Uint16 bufferCounter;
extern Uint32 bufferIndex;
extern Uint16 SignalProcessingIndex;
extern float freq_c;
extern float F_factor;
extern Uint16 reverbPercent;

//extern const int coeff[5*IIR16_NBIQ];
//extern long dataBuffer[2*IIR32_BPF_NBIQ];


/**
 *------------------------------------------------------------------------------
 * Public Constants (extern)
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Prototypes
 *------------------------------------------------------------------------------
 */

//void SignalProcess_LinearInterpolate(float * arrayToProcess);
//void SignalProcess_Decimation(float * arrayToProcess, Uint16 samplesToDecimate);
void SignalProcess_RealTimeReverb(int16 * x, float delayTimeSec, float a);
void SignalProcess_RealTimeEcho(int16 * x, int16 * y, float delayTimeSec, float a);
void SignalProcess_RealTimeTransform(int16 * x, float delayTimeSec, float a);
void SignalProcess_RealTimeHamming(int16 * x, float delayTimeSec, float a);
void SignalProcess_RealTimeRoll(int16 *y, Uint16 * output,  float delayTimeSec, float a);
void SignalProcess_RealTimePhasor(int16 *x, float a);
void SignalProcess_RealTimeFlanger(int16 *x, int16 *y, float delayTimeSec, float a);


//float SignalProcess_RealTimeFIR(FIR_STRUCT * x);
//float SignalProcess_RealTimeIIR(IIR_STRUCT * x);
void SignalProcess_Init(void);
//float SignalProcess_FIR(void *);
//float SignalProcess_IIR(void *);
//void LowPassFIR_Init(FIR_STRUCT *x);
//void BandPassFIR_Init(FIR_STRUCT *x);
//void HighPassFIR_Init(FIR_STRUCT *x);
//void SignalProcess_LowPassIIRInit(IIR5BIQ16 *x);
//void SignalProcess_BandPassIIRInit(IIR5BIQ32 *x);
//void SignalProcess_CustomPassIIRInit(IIR5BIQ16 *x);
void SignalProcess_RealTimeDFT(DFT_STRUCT *x);
void SignalProcess_DFTInit(DFT_STRUCT *x);



#endif /* SIGNAL_PROCESSING_BF_H_ */


################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../28335_RAM_lnk.cmd \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_headers/cmd/DSP2833x_Headers_nonBIOS.cmd 

ASM_SRCS += \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_ADC_cal.asm \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_usDelay.asm 

C_SRCS += \
../ADC_BF.c \
../DAC_BF.c \
../DMA_BF.c \
../DSP2833x_Adc.c \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_CpuTimers.c \
../DSP2833x_DMA.c \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_DefaultIsr.c \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_headers/source/DSP2833x_GlobalVariableDefs.c \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_Mcbsp.c \
../DSP2833x_PieCtrl.c \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_PieVect.c \
C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_SysCtrl.c \
../EffectsBox.c \
../Encoder.c \
../InternalADC_BF.c \
../LCD_Helper_Functions_BF.c \
../OneToOneI2CDriver.c \
../RotaryInput.c \
../SRAM_BF.c \
../Signal_Processing_BF.c \
../SwitchesLEDs_BF.c \
../Timer_BF.c \
../main.c 

OBJS += \
./ADC_BF.obj \
./DAC_BF.obj \
./DMA_BF.obj \
./DSP2833x_ADC_cal.obj \
./DSP2833x_Adc.obj \
./DSP2833x_CpuTimers.obj \
./DSP2833x_DMA.obj \
./DSP2833x_DefaultIsr.obj \
./DSP2833x_GlobalVariableDefs.obj \
./DSP2833x_Mcbsp.obj \
./DSP2833x_PieCtrl.obj \
./DSP2833x_PieVect.obj \
./DSP2833x_SysCtrl.obj \
./DSP2833x_usDelay.obj \
./EffectsBox.obj \
./Encoder.obj \
./InternalADC_BF.obj \
./LCD_Helper_Functions_BF.obj \
./OneToOneI2CDriver.obj \
./RotaryInput.obj \
./SRAM_BF.obj \
./Signal_Processing_BF.obj \
./SwitchesLEDs_BF.obj \
./Timer_BF.obj \
./main.obj 

ASM_DEPS += \
./DSP2833x_ADC_cal.d \
./DSP2833x_usDelay.d 

C_DEPS += \
./ADC_BF.d \
./DAC_BF.d \
./DMA_BF.d \
./DSP2833x_Adc.d \
./DSP2833x_CpuTimers.d \
./DSP2833x_DMA.d \
./DSP2833x_DefaultIsr.d \
./DSP2833x_GlobalVariableDefs.d \
./DSP2833x_Mcbsp.d \
./DSP2833x_PieCtrl.d \
./DSP2833x_PieVect.d \
./DSP2833x_SysCtrl.d \
./EffectsBox.d \
./Encoder.d \
./InternalADC_BF.d \
./LCD_Helper_Functions_BF.d \
./OneToOneI2CDriver.d \
./RotaryInput.d \
./SRAM_BF.d \
./Signal_Processing_BF.d \
./SwitchesLEDs_BF.d \
./Timer_BF.d \
./main.d 

C_DEPS__QUOTED += \
"ADC_BF.d" \
"DAC_BF.d" \
"DMA_BF.d" \
"DSP2833x_Adc.d" \
"DSP2833x_CpuTimers.d" \
"DSP2833x_DMA.d" \
"DSP2833x_DefaultIsr.d" \
"DSP2833x_GlobalVariableDefs.d" \
"DSP2833x_Mcbsp.d" \
"DSP2833x_PieCtrl.d" \
"DSP2833x_PieVect.d" \
"DSP2833x_SysCtrl.d" \
"EffectsBox.d" \
"Encoder.d" \
"InternalADC_BF.d" \
"LCD_Helper_Functions_BF.d" \
"OneToOneI2CDriver.d" \
"RotaryInput.d" \
"SRAM_BF.d" \
"Signal_Processing_BF.d" \
"SwitchesLEDs_BF.d" \
"Timer_BF.d" \
"main.d" 

OBJS__QUOTED += \
"ADC_BF.obj" \
"DAC_BF.obj" \
"DMA_BF.obj" \
"DSP2833x_ADC_cal.obj" \
"DSP2833x_Adc.obj" \
"DSP2833x_CpuTimers.obj" \
"DSP2833x_DMA.obj" \
"DSP2833x_DefaultIsr.obj" \
"DSP2833x_GlobalVariableDefs.obj" \
"DSP2833x_Mcbsp.obj" \
"DSP2833x_PieCtrl.obj" \
"DSP2833x_PieVect.obj" \
"DSP2833x_SysCtrl.obj" \
"DSP2833x_usDelay.obj" \
"EffectsBox.obj" \
"Encoder.obj" \
"InternalADC_BF.obj" \
"LCD_Helper_Functions_BF.obj" \
"OneToOneI2CDriver.obj" \
"RotaryInput.obj" \
"SRAM_BF.obj" \
"Signal_Processing_BF.obj" \
"SwitchesLEDs_BF.obj" \
"Timer_BF.obj" \
"main.obj" 

ASM_DEPS__QUOTED += \
"DSP2833x_ADC_cal.d" \
"DSP2833x_usDelay.d" 

C_SRCS__QUOTED += \
"../ADC_BF.c" \
"../DAC_BF.c" \
"../DMA_BF.c" \
"../DSP2833x_Adc.c" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_CpuTimers.c" \
"../DSP2833x_DMA.c" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_DefaultIsr.c" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_headers/source/DSP2833x_GlobalVariableDefs.c" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_Mcbsp.c" \
"../DSP2833x_PieCtrl.c" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_PieVect.c" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_SysCtrl.c" \
"../EffectsBox.c" \
"../Encoder.c" \
"../InternalADC_BF.c" \
"../LCD_Helper_Functions_BF.c" \
"../OneToOneI2CDriver.c" \
"../RotaryInput.c" \
"../SRAM_BF.c" \
"../Signal_Processing_BF.c" \
"../SwitchesLEDs_BF.c" \
"../Timer_BF.c" \
"../main.c" 

ASM_SRCS__QUOTED += \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_ADC_cal.asm" \
"C:/ti/controlSUITE/device_support/f2833x/v141/DSP2833x_common/source/DSP2833x_usDelay.asm" 



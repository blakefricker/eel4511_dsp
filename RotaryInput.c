/**
 * @file RotaryInput.c
 * @brief Sets up input for rotary switch.  Since rotary switch goes through an
 * encoder, the 6 inputs from the switch is condensed to 3 GPIO inputs.  This
 * file sets up the three inputs and decodes.
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "RotarySwitch.h"
#include "EffectsBox.h"
#include "Timer_BF.h"
#include "Signal_Processing_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

#define ROTARY_SWITCH_Y1_GPIO_PIN        (12)
#define ROTARY_SWITCH_Y2_GPIO_PIN        (13)
#define ROTARY_SWITCH_Y3_GPIO_PIN        (14)


/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

bool checkRotarySwitch;

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */


void RotarySwitch_Init(void)
{
    EALLOW;
    GpioCtrlRegs.GPAMUX1.all &= 0xC0FFFFFF; //Set GPIO 14:12 to zero to set pins to GPIO
    GpioCtrlRegs.GPADIR.all &= 0xFFFF8FFF;  //set GPIO 14:12 as input
    GpioCtrlRegs.GPAPUD.all &= 0xFFFF8FFF;  //Enable Pullups (set bit to zero)

    // setup high to low transition interrupts
    //GpioIntRegs.GPIOXINT3SEL.bit.GPIOSEL = ROTARY_SWITCH_Y1_GPIO_PIN;    // set GPIO12 as interrupt source for XINT3
    //GpioIntRegs.GPIOXINT4SEL.bit.GPIOSEL = ROTARY_SWITCH_Y2_GPIO_PIN;    // set GPIO13 as interrupt source for XINT4
    //GpioIntRegs.GPIOXINT5SEL.bit.GPIOSEL = ROTARY_SWITCH_Y3_GPIO_PIN;    // set GPIO14 as interrupt source for XINT5

    //PieVectTable.XINT3 = &rotarySwitchDecodeInput_isr;
    //PieVectTable.XINT4 = &rotarySwitchDecodeInput_isr;
    //PieVectTable.XINT5 = &rotarySwitchDecodeInput_isr;
    EDIS;

    // Configure falling edge
    //XIntruptRegs.XINT3CR.bit.POLARITY = 0;      // Falling edge interrupt
    //XIntruptRegs.XINT4CR.bit.POLARITY = 0;      // Falling edge interrupt
    //XIntruptRegs.XINT5CR.bit.POLARITY = 0;      // Falling edge interrupt

    // Enable XINT1 and XINT2
    //XIntruptRegs.XINT3CR.bit.ENABLE = 1;        // Enable Xint1
    //XIntruptRegs.XINT4CR.bit.ENABLE = 1;        // Enable XINT2
    //XIntruptRegs.XINT5CR.bit.ENABLE = 1;        // Enable XINT2

    // Enable Xint1 and XINT2 in the PIE: Group 1 interrupt 4 & 5
    // Enable int1 which is connected to WAKEINT:
    //PieCtrlRegs.PIECTRL.bit.ENPIE = 1;           // Enable the PIE block
    //PieCtrlRegs.PIEIER12.bit.INTx1 = 1;          // Enable PIE Group 1 INT1 12.1 -> XINT3
    //PieCtrlRegs.PIEIER12.bit.INTx2 = 1;          // Enable PIE Group 1 INT2 12.2 -> XINT4
    //PieCtrlRegs.PIEIER12.bit.INTx3 = 1;          // Enable PIE Group 1 INT3 12.3 -> XINT4
    //IER |= M_INT12;                              // Enable CPU int1

    //Init 3rd clock for trigger for input read
    //ConfigCpuTimer(&CpuTimer2, 149.92, (10000000));    //every 10ms
    //Timer2_Interrupt_Init();

    checkRotarySwitch = true;
}



void  RotarySwitch_DecodeInput(void)
{
    //decode the switch input
    Uint16 input = GpioDataRegs.GPADAT.all;
    input &= 0x7000;
    input >>= 12;

    EFFECT_STATE prevEffectState = effectState;

    switch(input)
    {
        case(7): // delay
                effectState = Effect_Delay;
                defaultEffectTime = EFFECT_DELAY_DEFAULT_TIME;
        break;
        case(6): // reverb
                effectState = Effect_Reverb;
                defaultEffectTime = EFFECT_REVERB_DEFAULT_TIME;
        break;
        case(5): // transform
                effectState = Effect_Transform;
                defaultEffectTime = EFFECT_TRANSFORM_DEFAULT_TIME;

        break;
        case(4): // pitch shifter
                effectState = Effect_PitchShifter;
                defaultEffectTime = EFFECT_PITCHSHIFTER_DEFAULT_TIME;
        break;
        case(3): //
                effectState = Effect_Flanger;
        defaultEffectTime = EFFECT_FLANGER_DEFAULT_TIME;
        break;
        case(2): //
                effectState = Effect_Roll;
                defaultEffectTime = EFFECT_ROLL_DEFAULT_TIME;
        break;
        case(1):
                effectState = Effect_Phasor;
                defaultEffectTime = EFFECT_PHASOR_DEFAULT_TIME;
    }

    if (effectState != prevEffectState)
    {
        updateToLCD = true;
        effectTime = defaultEffectTime;
        blockState = Block_C;
        reverbPercent = 50;
    }
}

/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

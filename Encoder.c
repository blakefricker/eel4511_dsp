/**
 * @file Encoder.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include <DSP28x_Project.h>
#include "Encoder.h"
#include "EffectsBox.h"
#include "Signal_Processing_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

#define ENCODER_A_GPIO_PIN      (48)
#define ENCODER_B_GPIO_PIN      (49)
#define TOLERANCE               (0.01)

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

void Encoder_Init(void)
{
    //Init GPIO Pins
    EALLOW;
    GpioCtrlRegs.GPBMUX2.all &= 0xFFFFFFF0;  //set GPIO49:48 to zero sets pins to GPIO
    GpioCtrlRegs.GPBDIR.all &= 0xFFFCFFFF;   //set GPIO49:48 as input
    GpioCtrlRegs.GPBPUD.all &= 0xFFFCFFFF;   //Enable Pullups (set bit to zero)

    //setup high to low transition interrupts
    GpioIntRegs.GPIOXINT6SEL.bit.GPIOSEL = ENCODER_A_GPIO_PIN - 32;  // set gpio as interrupt source for XINT6
    GpioIntRegs.GPIOXINT7SEL.bit.GPIOSEL = ENCODER_B_GPIO_PIN - 32;  // set gpio as interrupt source for XINT6

    PieVectTable.XINT6 = &EncoderMovement_isr;
    PieVectTable.XINT7 = &EncoderMovement_isr;
    EDIS;

    // Configure falling edge
    XIntruptRegs.XINT6CR.bit.POLARITY = 0;      // Falling edge interrupt
    XIntruptRegs.XINT7CR.bit.POLARITY = 0;      // Falling edge interrupt

    // Enable XINT1 and XINT2
    XIntruptRegs.XINT6CR.bit.ENABLE = 1;        // Enable XINT6
    XIntruptRegs.XINT7CR.bit.ENABLE = 1;        // Enable XINT7


    //Qualification Period to prevent bouncing
    EALLOW;
    GpioCtrlRegs.GPBCTRL.bit.QUALPRD2 = 255;
    GpioCtrlRegs.GPBQSEL2.bit.GPIO48 = 1;
    GpioCtrlRegs.GPBQSEL2.bit.GPIO49 = 1;
    EDIS;

    // Enable XINT6 and XINT7 in the PIE: Group 12 interrupt 6 & 7
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;          // Enable the PIE block
    PieCtrlRegs.PIEIER12.bit.INTx4 = 1;          // Enable PIE Group 1 INT4
    PieCtrlRegs.PIEIER12.bit.INTx5 = 1;          // Enable PIE Group 1 INT5
    IER |= M_INT12;


}

__interrupt void EncoderMovement_isr(void)
{
    for(Uint16 i = 0; i<10000; i++){}
    //decode the inputs
    Uint32 input = GpioDataRegs.GPBDAT.all;
    input &= 0x00030000;
    input >>= 16;


    if (input == 2) //turn was clockwise so increase
    {
        effectTime += .001;
        reverbPercent++;

        if (effectTime > EFFECT_MAX_TIME)
        {
            effectTime = EFFECT_MAX_TIME;
        }
        if (reverbPercent >= 100)
        {
            reverbPercent = 100;
        }
    }
    else if (input == 1 || input == 3) // turn was counter-clockwise so decrease
    {
        effectTime -= .001;
        if (effectTime < EFFECT_MIN_TIME)
        {
            effectTime = EFFECT_MIN_TIME;
        }
        reverbPercent--;
        if (reverbPercent <= 1)
        {
            reverbPercent = 1;
        }
    }

    if(effectTime <= (defaultEffectTime/8)*(1-TOLERANCE))
    {
        blockState = Block_LowOutside;
    }
    else if ((effectTime > ((defaultEffectTime/8)*(1-TOLERANCE))) && (effectTime < ((defaultEffectTime/8)*(1+TOLERANCE))))
    {
        blockState = Block_LLL;
    }
    else if ((effectTime > ((defaultEffectTime/8)*(1+TOLERANCE)))  && (effectTime < ((defaultEffectTime/4)*(1-TOLERANCE))))
    {
        blockState = Block_LLL_LL;
    }
    else if ((effectTime > ((defaultEffectTime/4)*(1-TOLERANCE)))  && (effectTime < ((defaultEffectTime/4)*(1+TOLERANCE))))
    {
        blockState = Block_LL;
    }
    else if ((effectTime > ((defaultEffectTime/4)*(1+TOLERANCE)))  && (effectTime < ((defaultEffectTime/2)*(1-TOLERANCE))))
    {
        blockState = Block_LL_L;
    }
    else if ((effectTime > ((defaultEffectTime/2)*(1-TOLERANCE))  && effectTime < (defaultEffectTime/2)*(1+TOLERANCE)))
    {
        blockState = Block_L;
    }
    else if ((effectTime > ((defaultEffectTime/2)*(1+TOLERANCE))  && effectTime < defaultEffectTime*(1-TOLERANCE)))
    {
        blockState = Block_L_C;
    }
    else if ((effectTime > (defaultEffectTime*(1-TOLERANCE))  && effectTime < defaultEffectTime*(1+TOLERANCE)))
    {
        blockState = Block_C;
    }
    else if ((effectTime > (defaultEffectTime*(1+TOLERANCE))  && effectTime < (defaultEffectTime*2)*(1-TOLERANCE)))
    {
        blockState = Block_C_R;
    }
    else if ((effectTime > ((defaultEffectTime*2)*(1-TOLERANCE))  && effectTime < (defaultEffectTime*2)*(1+TOLERANCE)))
    {
        blockState = Block_R;
    }
    else if ((effectTime > ((defaultEffectTime*2)*(1+TOLERANCE))  && effectTime < (defaultEffectTime*4)*(1-TOLERANCE)))
    {
        blockState = Block_R_RR;
    }
    else if ((effectTime > ((defaultEffectTime*4)*(1-TOLERANCE))  && effectTime < (defaultEffectTime*4)*(1+TOLERANCE)))
    {
        blockState = Block_RR;
    }
    else if ((effectTime > ((defaultEffectTime*4)*(1+TOLERANCE))  && effectTime < (defaultEffectTime*8)*(1-TOLERANCE)))
    {
        blockState = Block_RR_RRR;
    }
    else if ((effectTime > ((defaultEffectTime*8)*(1-TOLERANCE))  && effectTime < (defaultEffectTime*8)*(1+TOLERANCE)))
    {
        blockState = Block_RRR;
    }
    else if (effectTime >= ((defaultEffectTime*8)*(1+TOLERANCE)))
    {
        blockState = Block_HighOutside;

    }


    updateToLCD = true;

    PieCtrlRegs.PIEIFR12.bit.INTx4 = 0;
    PieCtrlRegs.PIEIFR12.bit.INTx5 = 0;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}
/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

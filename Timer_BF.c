/**
 * @file Timer_BF.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "common_header.h"
#include "Timer_BF.h"
#include "Signal_Processing_BF.h"
#include "RotarySwitch.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */


/**
 * @brief timer period values
 */
#define     Freq_2_5KHz     (0xEA40)
#define     Freq_5KHz       (0x7520)
#define     Freq_10KHz      (0x3A90)
#define     Freq_15KHz      (0x270A)
#define     Freq_20KHz      (0x1D48)
#define     Freq_25KHz      (0x1866)
#define     Freq_30KHz      (0x1385)
#define     Freq_35KHz      (0x10BA)
#define     Freq_40KHz      (0x0EA4)
#define     Freq_44100Hz    (0x0D4D)
#define     Freq_60KHz      (0x09C2)


/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

Uint16 delayIndex1;
Uint16 delayIndex2;

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

Uint16 Check_Condition = 0;
TIMER_STRUCT TimerInUse;
bool countingUp;

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

///**
// * @brief Initialize Timer1
// * @param mSec is time to set timer for in milliseconds
// */
//void Timer1_Init_mSec(Uint32 mSec)
//{
//    CpuTimer1Regs.TPR.bit.TDDR = 14999;   // Timer-Divide Down = 100Hz
//    CpuTimer1Regs.PRD.all = (mSec * 492);
//    CpuTimer1Regs.TCR.bit.TRB = 1;          //Load Prescaler and Period
//    CpuTimer1Regs.TCR.bit.TIE = 1;
//}
//
//
///**
// * @brief Initialize Timer1
// * @param uSec is time to set timer for in microseconds
// */
//void Timer1_Init_uSec(float uSec)
//{
//    float period = (uSec * 149.92);
//    CpuTimer1Regs.TPR.bit.TDDR = 0;
//    CpuTimer1Regs.PRD.all = (Uint32)(period);
//    CpuTimer1Regs.TCR.bit.TRB = 1;          //Load Prescaler and Period
//    CpuTimer1Regs.TCR.bit.TIE = 1;
//}
//

void Timer1_Init_Gen(TIMER_STATE timerState)
{
    Uint32 val = 0;
    switch(TimerInUse.timerState)
    {
    case(Timer_2_5KHz):
    TimerInUse.timerFreq = 2500;
    val = Freq_2_5KHz;
    break;

    case(Timer_5KHz):
    TimerInUse.timerFreq = 5000;
    val = Freq_5KHz;
    break;

    case(Timer_10KHz):
    TimerInUse.timerFreq = 10000;
    val = Freq_10KHz;
    break;

    case(Timer_15KHz):
    TimerInUse.timerFreq = 15000;
    val = Freq_15KHz;
    break;

    case(Timer_20KHz):
    TimerInUse.timerFreq = 20000;
    val = Freq_20KHz;
    break;

    case(Timer_25KHz):
    TimerInUse.timerFreq = 25000;
    val = Freq_25KHz;
    break;

    case(Timer_30KHz):
    TimerInUse.timerFreq = 30000;
    val = Freq_25KHz;
    break;

    case(Timer_35KHz):
    TimerInUse.timerFreq = 35000;
    val = Freq_25KHz;
    break;

    case(Timer_40KHz):
    TimerInUse.timerFreq = 40000;
    val = Freq_25KHz;
    break;

    case(Timer_44100Hz):
    TimerInUse.timerFreq = 44100;
    val = Freq_44100Hz;
    break;
    }

    CpuTimer1Regs.TPR.bit.TDDR = 0;
    CpuTimer1Regs.PRD.all = (val);
    CpuTimer1Regs.TCR.bit.TRB = 1;          //Load Prescaler and Period
    CpuTimer1Regs.TCR.bit.TIE = 1;
}

///**
// * @brief Initialize Timer1 Interrupt
// */
//void Timer1_Interrupt_Init(void)
//{
//    EALLOW;
//    PieVectTable.XINT13 = &cpu_timer1_isr;
//    CpuTimer1Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
//    EDIS;
//    IER |= M_INT13;      // Enable interrupt 13 which is connected to timer 1
//    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;    // Enable TINT0 in the PIE: Group 1 interrupt 7
//}


/**
 * @brief Initialize Timer0 Interrupt
 */
void Timer0_Interrupt_Init(void)
{
    EALLOW;
    PieVectTable.TINT0 = &cpu_timer0_isr;
    CpuTimer0Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
    EDIS;
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
    IER |= M_INT1;       // Enable CPU interrupt 1 which is connected to timer 0

    countingUp = 0;
}

/**
 * @brief Initialize Timer2 Interrupt
 */
void Timer2_Interrupt_Init(void)
{
    EALLOW;
    PieVectTable.TINT2 = &cpu_timer2_isr;
    CpuTimer2Regs.TCR.all = 0x4000; // Use write-only instruction to set TSS bit = 0
    EDIS;
    IER |= M_INT14;
}

/**
 * @brief Interrupt for Timer0
 * @details Using xint1
 */
__interrupt void cpu_timer0_isr(void)
{
    delayIndex1++;
    delayIndex1 %= 1025;
    delayIndex2++;
    delayIndex2 %= 1025;

    if (countingUp)
    {
        freq_c += 1;
        if (freq_c >= 3000)
        {
            countingUp = false;
        }
    }
    else
    {
        freq_c -= 1;
        if (freq_c <= 100)
        {
            countingUp = true;
        }
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

///**
// * @brief Interrupt for Timer1
// * @details Using xint13
// */
//__interrupt void cpu_timer1_isr(void)
//{
//    Check_Condition = 1;
//}



__interrupt void cpu_timer2_isr(void)
{
    checkRotarySwitch = true;
}


/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */


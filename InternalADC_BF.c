/**
 * @file InternalADC_BF.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "InternalADC_BF.h"
#include "EffectsBox.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

void InternalADC_Init()
{
    // Define ADCCLK clock frequency ( less than or equal to 25 MHz )
    // Assuming InitSysCtrl() has set SYSCLKOUT to 150 MHz
    EALLOW;
    SysCtrlRegs.HISPCP.all = 3;
    EDIS;

    InitAdc();

    AdcRegs.ADCTRL1.bit.ACQ_PS = 0;
    AdcRegs.ADCTRL1.bit.SEQ_CASC = 0;        // 0 Non-Cascaded Mode
    AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 0x1;  // Enable SEQ1 interrupt (every EOS)
    AdcRegs.ADCTRL2.bit.RST_SEQ1 = 0x1;
    AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0;    // Set up ADC to perform 1 conversion for every SOC
    AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0x0;

    EALLOW;
    PieVectTable.SEQ1INT = &adc_isr;
    EDIS;
    PieCtrlRegs.PIEIER1.bit.INTx1 = 1;

    IER |= M_INT1 ;                                //Enable INT7 (7.1 DMA Ch1)
}


void updateEffectVariable()
{
    //Trigger an ADC conversin and wait for ADC ISR
    AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 0x1;
}


__interrupt void  adc_isr(void)
{
  Uint16 inputVal = AdcRegs.ADCRESULT0;
  effectVolume = (((float)inputVal-0x0C00) / (0xFFF0-0xC00));     // effectVolume should be between 0-1

  if (effectVolume < 0)
  {
      effectVolume = 0.0;
  }

  // Reinitialize for next ADC sequence
  AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;         // Reset SEQ1
  AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       // Clear INT SEQ1 bit
  PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

  return;
}
/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

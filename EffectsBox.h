/**
 * @file EffectsBox.h
 * @brief TODO:
 * Author: Blake Fricker
 */

#ifndef EFFECTSBOX_H_
#define EFFECTSBOX_H_

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "DSP28X_Project.h"
#include "common_header.h"
#include "PingPongBuffer.h"

/**
 *------------------------------------------------------------------------------
 * Public Defines
 *------------------------------------------------------------------------------
 */
#define BUFFER_MULTIPLIER            (4096.0)
#define BUFFER_ARRAY_LENGTH         (Uint32)(PINGPONG_BUFFER_LENGTH*BUFFER_MULTIPLIER)

#define EFFECT_PHASOR_DEFAULT_TIME          (2)
#define EFFECT_FLANGER_DEFAULT_TIME         (2)
#define EFFECT_REVERB_DEFAULT_TIME          (0.5)
#define EFFECT_DELAY_DEFAULT_TIME           (0.5)
#define EFFECT_TRANSFORM_DEFAULT_TIME       (0.5)
#define EFFECT_ROLL_DEFAULT_TIME            (0.5)
#define EFFECT_PITCHSHIFTER_DEFAULT_TIME    (0.5)
#define EFFECT_MAX_TIME                     (11.8)      //(32768 * 2)/ SamplingFreq
#define EFFECT_MIN_TIME                     (.000025)//25ns

#define EFFECT_ROLL_MAX_INDEX               (BUFFER_ARRAY_LENGTH)
/**
 *------------------------------------------------------------------------------
 * Public Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Data Types
 *------------------------------------------------------------------------------
 */


typedef enum
{
    Effect_Delay    =   (0x00),
    Effect_Phasor,
    Effect_Flanger,
    Effect_Reverb,
    Effect_Transform,
    Effect_Roll,
    Effect_PitchShifter,
    Effect_NumerOfStates
}EFFECT_STATE;

typedef enum
{
    Block_LowOutside =  (0x00),
    Block_LLL,
    Block_LLL_LL,
    Block_LL,
    Block_LL_L,
    Block_L,
    Block_L_C,
    Block_C,
    Block_C_R,
    Block_R,
    Block_R_RR,
    Block_RR,
    Block_RR_RRR,
    Block_RRR,
    Block_HighOutside,
    Block_NumberOfStates
}BLOCK_STATE;

/**
 *------------------------------------------------------------------------------
 * Public Variables (extern)
 *------------------------------------------------------------------------------
 */

extern Uint32 FFT_DelayCounter;
extern Uint16 BPM;
extern float effectVolume;
extern float effectTime;
extern float maxEffectTime;
extern float minEffectTime;
extern float defaultEffectTime;
extern bool effectOn;
extern bool reloadInputArray;
extern bool runDFT;
extern bool updateToLCD;
extern bool showBars;
extern Uint32 rollStartIndex;
extern Uint32 rollEndIndex;
extern Uint32 rollIndex;
extern Uint32 samplesPerChunk;
extern EFFECT_STATE effectState;
extern BLOCK_STATE blockState;
extern Uint16 effectIndex;
extern int16 inputBuffer[BUFFER_ARRAY_LENGTH];
extern int16 outputBuffer[BUFFER_ARRAY_LENGTH];

/**
 *------------------------------------------------------------------------------
 * Public Constants (extern)
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Prototypes
 *------------------------------------------------------------------------------
 */

void EffectsBox_Init(void);

#endif /* EFFECTSBOX_H_ */


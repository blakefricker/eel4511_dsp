/**
 * @file SwitchesLEDs_BF.h
 * @brief TODO:
 * Author: Blake Fricker
 */

#ifndef SWITCHESLEDS_BF_H_
#define SWITCHESLEDS_BF_H_

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Defines
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables (extern)
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Constants (extern)
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Function Prototypes
 *------------------------------------------------------------------------------
 */

void SwitchesLEDsInit(void);
void sendToLEDs(int value);
int returnSwitchValue(void);
Uint16 checkSwitches(void);
void GPIO_InitDiagPort(Uint16 portNumber);
void GPIO_Toggle(Uint16 bitNumber);
void GPIO13_SetLow();
void GPIO13_SetHigh();

void Buttons_Init(void);
__interrupt void upButtonPressed_isr(void);
__interrupt void downButtonPressed_isr(void);
__interrupt void onOffButtonPressed_isr(void);


#endif /* SWITCHESLEDS_BF_H_ */


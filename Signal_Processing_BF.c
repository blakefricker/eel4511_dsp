/**
 * @file Signal_Processing_BF.c
 * @brief TODO:
 * Author: Blake Fricker
 */

/**
 *------------------------------------------------------------------------------
 * Include Files
 *------------------------------------------------------------------------------
 */

#include "Signal_Processing_BF.h"
#include "Timer_BF.h"
#include "SwitchesLEDs_BF.h"
#include "hammingWindow.h"
//#include "Coefficients.h"
#include <math.h>
#include "PingPongBuffer.h"
#include "EffectsBox.h"
#include "DMA_BF.h"

/**
 *------------------------------------------------------------------------------
 * Private Defines
 *------------------------------------------------------------------------------
 */

/** @brief Defines (x-x0)/(x1-x0) = 1/2 for Linear Interpoloation */
//#define mu      (.5)
#define pi      (3.14)

/**
 *------------------------------------------------------------------------------
 * Private Macros
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Private Data Types
 *------------------------------------------------------------------------------
 */

/**
 *------------------------------------------------------------------------------
 * Public Variables
 *------------------------------------------------------------------------------
 */


//#pragma DATA_SECTION(x_mag, ".SRAMData")
float x_mag[SignalProcessingMaxIndex];
//#pragma DATA_SECTION(x_imag, ".SRAMData")
float x_imag[SignalProcessingMaxIndex];
//#pragma DATA_SECTION(x_real, ".SRAMData")
float x_real[SignalProcessingMaxIndex];

Uint16 SignalProcessingIndex;
Uint16 bufferCounter;
Uint32 bufferIndex;
float freq_c;
float F_factor;
Uint16 reverbPercent;

/**
 *------------------------------------------------------------------------------
 * Private Variables
 *------------------------------------------------------------------------------
 */

/* Define the Delay buffer for the cascaded 6 biquad IIR filter and place it in "iirfilt" section */
//#pragma DATA_SECTION(dataBuffer,"iirldb");
//int dataBuffer[2*IIR16_LP_NBIQ];
//long dataBuffer[2*IIR32_BPF_NBIQ];
Uint32 effectCounter;
bool transformActive;
float Q_factor;

float Y_H[PINGPONG_BUFFER_LENGTH];
float Y_B[PINGPONG_BUFFER_LENGTH];
float Y_L[PINGPONG_BUFFER_LENGTH];
/**
 *------------------------------------------------------------------------------
 * Public Constants
 *------------------------------------------------------------------------------
 */


/**
 *------------------------------------------------------------------------------
 * Private Constants
 *------------------------------------------------------------------------------
 */

//const int coeff[5*IIR16_LPF_NBIQ]=IIR16_LP_COEFF;
//const long coeff[5*IIR32_BPF_NBIQ]=IIR32_BPF_COEFF;

/**
 *------------------------------------------------------------------------------
 * Private Function Prototypes
 *------------------------------------------------------------------------------
 */

//static float linearInterpolate(float value1, float value2);

/**
 *------------------------------------------------------------------------------
 * Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * @brief Initializes variables for performing Signal Processing
 */
void SignalProcess_Init(void)
{
    /* Used for Hamming Filter */
    delayIndex1 = 0;
    delayIndex2 = 512;

    SignalProcessingIndex = 0;
    effectCounter = 0;
    transformActive = false;
    F_factor = 0;
    Q_factor = (.8);     // 2 * damping factor
    freq_c = 100;
    reverbPercent = 50;

    for (Uint32 i = 0; i < SignalProcessingMaxIndex; i++)
    {
        x_imag[i] = 0;
        x_real[i] = 0;
        x_mag[i] = 0;
        Y_L[i] = 0;
        Y_B[i] = 0;
        Y_H[i] = 0;
    }

}

/**
 * @brief Linear Interpolate Array
 * @param Pointer to array to be interpolated.
 * @details Starts in middle of array, finds two points, then saves the new
 *    3 points starting the back of the array working forward.  This way a new
 *    temp array doesn't need to be created.
 */
//void SignalProcess_LinearInterpolate(float * arrayToProcess)
//{
//    Uint32 sourceIndex = (SignalProcessingMaxIndex / 2) -1;  // start at end of first half
//    Uint32 destIndex = SignalProcessingMaxIndex -2;          // -1 for index and for -1 for interpolation result
//
//    arrayToProcess[destIndex--] = arrayToProcess[sourceIndex--];
//    for (Uint32 i = sourceIndex+1; i > 0; i--)
//    {
//        arrayToProcess[destIndex--] = linearInterpolate(arrayToProcess[sourceIndex], arrayToProcess[sourceIndex+1]);
//        arrayToProcess[destIndex--] = arrayToProcess[sourceIndex--];
//    }
//}
//
///**
// * @brief Decimate Array
// * @param Pointer to array to be decimated.
// */
//void SignalProcess_Decimation(float * arrayToProcess, Uint16 samplesToDecimate)
//{
//    Uint32 dest = 1;
//    while((dest*(1+samplesToDecimate)) < (SignalProcessingMaxIndex))
//    {
//        arrayToProcess[dest] = arrayToProcess[dest * (samplesToDecimate + 1)];
//        dest++;
//    }
//}

/**
 * @brief Real Time Reverb Effect
 * @param arrayToProcess Pointer to data array of data stream
 * @param delayTimeSec Reverb delay time in seconds
 * @param a Volume of the reverb.
 * @return Current output value for current reverb.
 * @detais This is real time effect, meaning that the data will be streaming
 *     into the data array and the value that gets send out to the DAC will come
 *     directly from this function.  Once the data array has enough values to handle
 *     the sample delay, it will start playing.
 *
 *     Based on the formula y[n] = (1-a)x[n] + ax[n-p] and p is the delay time
 *     in samples.   p = [t * Fs]
 */
void SignalProcess_RealTimeReverb(int16 * x, float delayTimeSec, float a)
{
    float p = (reverbPercent * .000706 * TimerInUse.timerFreq); //(delayTimeSec * TimerInUse.timerFreq);
    int32 scratch = (bufferIndex - p);

    //This while loop will setup the [n-p] correctly so that when the difference
    //is negative is will correct.
    while (scratch < 0)
    {
        scratch += BUFFER_ARRAY_LENGTH;
    }
    outputBuffer[bufferIndex] = (1-a)*x[bufferIndex] + (a *x[scratch]);
}

/**
 * @brief Real Time Echo Effect
 * @param x Pointer to data array of data stream or x[n].
 * @param y Pointer to data array for effect or y[n].
 * @param delayTimeSec Echo delay time in seconds
 * @param a Volume of the Echo
 * @return Current output value for current Echo.
 * @detais This is real time effect, meaning that the data will be streaming
 *     into the data array and the value that gets send out to the DAC will come
 *     directly from this function.  Once the data array has enough values to handle
 *     the sample delay, it will start playing.
 *
 *     Based on the formula y[n] = (1-a)x[n] + ay[n-p] and p is the delay time
 *     in samples.   p = [t * Fs]
 */
void SignalProcess_RealTimeEcho(int16 * x, int16 * y, float delayTimeSec, float a)
{
    float p = (delayTimeSec * TimerInUse.timerFreq);
    int32 scratch = (bufferIndex - p);

    if (scratch < 0)
    {
        scratch += BUFFER_ARRAY_LENGTH;
    }
    outputBuffer[bufferIndex] = (x[bufferIndex] + (a *y[scratch]));
}

void SignalProcess_RealTimeTransform(int16 * x, float delayTimeSec, float a)
{
    int32 samplesPerChunk = ((TimerInUse.timerFreq * delayTimeSec) / 4.0);
    if (transformActive)
    {
        outputBuffer[bufferIndex] = (1-a)*x[bufferIndex]; // + 0 for the effect part.
        effectCounter++;
        if (effectCounter >= samplesPerChunk)
        {
            transformActive = false;
            effectCounter = 0;
        }
    }
    else
    {
        outputBuffer[bufferIndex] = x[bufferIndex];
        effectCounter++;
        if (effectCounter >= samplesPerChunk)
        {
            transformActive = true;
            effectCounter = 0;
        }
    }
}

/**
 * @brief Real-Time Hamming Effect
 * @param x Current input array to be processed
 * @return returns a value representing y[n] that should be sent to the DAC
 *  and not saved.
 * @details
 *      y[n] = hamming[delay1] * x[n-delay1] + hamming[delay2] * x[n-delay2]
 */
void SignalProcess_RealTimeHamming(int16 * x, float delayTimeSec, float a)
{
    CpuTimer0Regs.PRD.all = (Uint32)(TimerInUse.timerFreq * (delayTimeSec));

    Uint32 delay1 = bufferIndex - delayIndex1;
    delay1 %= BUFFER_ARRAY_LENGTH;
    Uint32 delay2 = bufferIndex - delayIndex2;
    delay2 %= BUFFER_ARRAY_LENGTH;

    float y = (hamming[delayIndex1] * x[bufferIndex - delayIndex1]) + (hamming[delayIndex2] * x[bufferIndex - delayIndex2]);
    outputBuffer[bufferIndex] = (1-a)*x[bufferIndex] + a*y;
}


void SignalProcess_RealTimeRoll(int16 *y, Uint16 * out,  float delayTimeSec, float a)
{
    int32 samplesPerChunk = (TimerInUse.timerFreq * delayTimeSec);
    rollEndIndex = (Uint32)(((int32)((float)rollStartIndex + samplesPerChunk)) & BUFFER_ARRAY_LENGTH-1); //% BUFFER_ARRAY_LENGTH);

    out[SignalProcessingIndex] = y[rollIndex++]+0x7FFF;//((1-a)*x[bufferIndex]  + 0x7FFF);//+ a*outputBuffer[scratch]

    if (rollIndex == EFFECT_ROLL_MAX_INDEX)
    {
        rollIndex = 0;
    }
    else if (rollIndex == rollEndIndex)
    {
        rollIndex = rollStartIndex;
    }
}

/**
 * @brief Real-Time Wah-Wah Filter
 * @param x Current input array to be processed
 * @return Output Value
 */
void SignalProcess_RealTimePhasor(int16 *x, float a)
{
    float p = (F_factor * (TimerInUse.timerFreq/200));
    int32 scratch = (bufferIndex - p);

    if (scratch < 0 )
    {
        scratch += BUFFER_ARRAY_LENGTH;
    }

    outputBuffer[bufferIndex] = (x[bufferIndex] + (a*x[scratch]));// * (-.5*a +1); //((-.5*a +1) -> y = -.5x+1

    //outputBuffer[bufferIndex] = (x[bufferIndex] + (a*((x[bufferIndex] + x[(Uint32)scratch])/2)));


//    int32 delayIndex = SignalProcessingIndex-1;
//    if (delayIndex < 0)
//    {
//        delayIndex += SignalProcessingMaxIndex;
//    }
//
//    //calculate Y_H[n] = -YL[n-1] + x[n] - q*YB[n-1]
//    Y_H[SignalProcessingIndex] = (float)x[bufferIndex] - (Q_factor*Y_B[delayIndex]) - Y_L[delayIndex];
//
//    //calculate Y_B[n] = F*YH[n] + YB[n-1]
//    Y_B[SignalProcessingIndex] = (F_factor * Y_H[SignalProcessingIndex]) + Y_B[delayIndex];
//
//    //calculate Y_L[n] = F*YB[n] + YL[n-1]
//    Y_L[SignalProcessingIndex] = (F_factor * Y_B[SignalProcessingIndex]) + Y_L[delayIndex];
//
//    outputBuffer[bufferIndex] = (int16)((1-a)*x[bufferIndex] + (a * Y_L[SignalProcessingIndex]+Y_H[SignalProcessingIndex]));
}

void SignalProcess_RealTimeFlanger(int16 *x, int16 *y, float delayTimeSec, float a)
{
    float p = (F_factor * (TimerInUse.timerFreq/100));
    int32 scratch = (bufferIndex - p);

    if (scratch < 0 )
    {
        scratch += BUFFER_ARRAY_LENGTH;
    }
    outputBuffer[bufferIndex] = (x[bufferIndex] + (a*y[scratch]));
}

/**
 * @brief Real-Time FIR Filter
 * @param x Current input array to be processed
 * @return Output value
 */
//float SignalProcess_RealTimeFIR(FIR_STRUCT *x)
//{
//    float y = 0;
//    int16 scratch;
//
//    for (int i = 0; i < x->taps; i++)
//    {
//        scratch = x->circularBuffer_Index - i;
//        if (scratch < 0)
//        {
//            scratch += x->circularBuffer_MaxIndex;
//        }
//        y += x->coefficients_ptr[i] * x->circularBuffer_ptr[scratch];
//    }
//    return y;
//}

/**
 * @brief Real-Time IIR Filter
 * @param x Current input array to be processed
 * @return Output Value
 */
//float SignalProcess_RealTimeIIR(IIR_STRUCT *x)
//{
//    float y = 0;
//    int16 index;
//
//    for (int i = 0; i < x->taps; i++)
//    {
//        // fix index
//        index = x->circularBuffer_Index - i;
//        if (index < 0)
//        {
//            index += x->circularBuffer_MaxIndex;
//        }
//
//        //b[i]*x[n-i] - a[i]*y[n-i]         index = n-i
//        y += x->coefficientsB_ptr[i]*x->circularBuffer_ptr[index];
//    }
//
//    for (Uint16 i = 1; i < x->taps; i++)
//    {
//        // fix index
//        index = x->circularBuffer_Index - i;
//        if (index < 0)
//        {
//            index += x->circularBuffer_MaxIndex;
//        }
//
//        y -= x->coefficientsA_ptr[i]*x->outputBuffer_ptr[index];
//    }
//
//    return (y*x->coefficientsA_ptr[0]);
//}

///**
// * @brief Initialize Low Pass FIR
// * @param Pointer to FIR Struct
// */
//void LowPassFIR_Init(FIR_STRUCT *x)
//{
//    x->coefficients_ptr = lowPassFIRCoefficients;
//    x->taps = LOWPASS_FIR_TAPS;
//    x->circularBuffer_ptr = inputArray_X;
//}
//
///**
// * @brief Initialize Band Pass FIR
// * @param Pointer to FIR Struct
// */
//void BandPassFIR_Init(FIR_STRUCT *x)
//{
//    x->coefficients_ptr = bandPassFIRCoefficients;
//    x->taps = BANDPASS_FIR_TAPS;
//    x->circularBuffer_ptr = inputArray_X;
//}
//
///**
// * @brief Initialize High Pass FIR
// * @param Pointer to FIR Struct
// */
//void HighPassFIR_Init(FIR_STRUCT *x)
//{
//    x->coefficients_ptr = highPassFIRCoefficients;
//    x->taps = HIGHPASS_FIR_TAPS;
//    x->circularBuffer_ptr = inputArray_X;
//}
//
///**
// * @brief Initialize Low Pass IIR
// * @param Pointer to IIR Struct
// */
////void SignalProcess_LowPassIIRInit(IIR5BIQ16 *x)
////{
////    x->dbuffer_ptr=dataBuffer;
////    x->coeff_ptr=(int *)coeff;
////    x->qfmat=IIR16_LP_QFMAT;
////    x->nbiq=IIR16_LP_NBIQ;
////    x->isf=IIR16_LP_ISF;
////    //x->init(&x);
////}
//
///**
// * @brief Initialize Band Pass IIR
// * @param Pointer to IIR Struct
// */
////void SignalProcess_BandPassIIRInit(IIR5BIQ32 *x)
////{
////    x->dbuffer_ptr=dataBuffer;
////    x->coeff_ptr=(long *)coeff;
////    x->qfmat=IIR32_BPF_QFMAT;
////    x->nbiq=IIR32_BPF_NBIQ;
////    x->isf=IIR32_BPF_ISF;
////    //x->init(&x);
////}
//
/////**
//// * @brief Initialize Band Pass IIR
//// * @param Pointer to IIR Struct
//// */
////void SignalProcess_CustomIIRInit(IIR5BIQ16 *x)
////{
////    x->dbuffer_ptr=dbuffer;
////    x->coeff_ptr=(int *)coeff;
////    x->qfmat=IIR16_CPF_QFMAT;
////    x->nbiq=IIR16_CPF_NBIQ;
////    x->isf=IIR16_CPF_ISF;
////    x->init(&x);
////}

/**
 * @bfrief Initialize DFT
 */
void SignalProcess_DFTInit(DFT_STRUCT *x)
{
    x->imag = x_imag;
    x->real = x_real;
    x->mag = x_mag;
    x->input= inputBuffer;
    x->maxMag = 0;
    x->N = SignalProcessingMaxIndex;
}

/**
 * @brief Real-Time 256 Point DFT
 * @param DFT_STRUCT handle
 */
void SignalProcess_RealTimeDFT(DFT_STRUCT *x)
{
    x->maxFreq=0;
    x->maxMag =0;

    float theta = 0;
    for(Uint16 i = 0; i < x->N; i++)
    {
        x->real[i] = 0;
        x->imag[i] = 0;
        x->mag[i] = 0;

        for(Uint16 j = 0; j < x->N; j++)
        {
            theta = (-2*pi*i*j)/x->N;
            x->real[i] += x->input[j] * cosf(theta);
            x->imag[i] += x->input[j] * sinf(theta);
        }
        // avoid taking square root by doing 10 log (x^2) instead of  20 log (x)
        x->mag[i] = x->real[i] * x->real[i] + x->imag[i] * x->imag[i];

        //Now find max;
        if ((x->mag[i] > x->maxMag) && (i <= 128))
        {
            x->maxMag = x->mag[i];
            x->maxIndex = i;
        }
    }
    x->maxMag = 10 * log10f(x->maxMag);

    // max frequency = index * fs/N      Note: for 128 shift 7, 256 shift 8
    x->maxFreq = (TimerInUse.timerFreq >> 8) * x->maxIndex;


}

/**
 *------------------------------------------------------------------------------
 * Private Function Definitions
 *------------------------------------------------------------------------------
 */

///**
// * @brief Interpolation Function
// * @param todo
// * @param todo
// * @return todo
// */
//static float linearInterpolate(float value1, float value2)
//{
//    return (value1*(1-mu) + value2*mu);
//}
